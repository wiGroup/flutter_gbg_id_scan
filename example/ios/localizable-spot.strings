/* 
  Localizable.strings
  MJCS

  Copyright © 2020 GB Group Plc. All rights reserved.
*/

"MJCS_no_rectangle" = "We can’t find your document, please capture it again";
"MJCS_journey_action_front" = "Please capture the page of your document that has your picture and personal info";
"MJCS_journey_action_back" = "Please capture back side of your document";
"MJCS_journey_action_selfie" = "Please capture your selfie";
"MJCS_journey_action_poa" = "Please capture your proof of address document";
"MJCS_journey_action_rescan" = "Please try again";

"MJCS_extraction_confirm_finish" = "Finish";
"MJCS_extraction_confirm_cancel" = "Cancel";

"MJCS_progress_upload_title" = "Uploading…";
"MJCS_progress_upload_message" = "Please wait a few seconds…";
"MJCS_progress_unreachable_title" = "Connection error";
"MJCS_progress_unreachable_label" = "Please tap anywhere to retry again";
"MJCS_progress_action_retry" = "Retry";

"MJCS_scanning_message" = "Position your ID document inside the frame";
"MJCS_glare_detected" = "Glare detected";
"MJCS_blur_detected" = "Blur detected";
"MJCS_low_resolution_detected" = "Move your document closer";
"MJCS_document_boundary_detected" = "Position your ID document inside the frame";

"MJCS_action_liveness_front" = "Look straight at the camera";
"MJCS_action_liveness_up" = "Tilt your face up slightly - not too much";
"MJCS_action_liveness_down" = "Tilt your face down slightly - not too much";
"MJCS_action_liveness_left" = "Turn your face left slightly - not too much";
"MJCS_action_liveness_right" = "Turn your face right slightly - not too much";
"MJCS_action_liveness_smile" = "Smile";
"MJCS_action_liveness_frown" = "Frown";
"MJCS_action_liveness_none" = "Don’t move";
"MJCS_action_liveness_title" = "Liveness Detection";
"MJCS_action_liveness_subtitle" = "We need to check that no one is trying to impersonate you.";
"MJCS_action_liveness_message" = "Keep your face inside the frame, look straight at the camera and follow the instructions.";
"MJCS_action_liveness_calibrate_up" = "Move the device slightly up";
"MJCS_action_liveness_calibrate_down" = "Move the device slightly down";
"MJCS_action_liveness_calibrate_left" = "Move the device slightly left";
"MJCS_action_liveness_calibrate_right" = "Move the device slightly right";
"MJCS_action_liveness_calibrate_proceed" = "Proceed";

"MJCS_journey_action_passive_liveness_title" = "Keep your face inside the frame";
"MJCS_journey_action_selfie_message" = "Keep your eyes on the screen";
"MJCS_passive_liveness_start_title" = "Liveness Detection";
"MJCS_passive_liveness_start_message" = "We need to check that no one is trying to impersonate you";
"MJCS_passive_liveness_start_bottom_message" = "Keep your face inside the frame, look straight at the camera and follow the instructions.";
"MJCS_passive_liveness_uploading_title" = "Just a moment…";
"MJCS_passive_liveness_uploading_message" = "Performing liveness checks, won't be long";
"MJCS_passive_liveness_completed_title" = "Completed";
"MJCS_passive_liveness_completed_message" = "You have completed all checks";
"MJCS_passive_liveness_start_button_title" = "Start";
"MJCS_passive_liveness_error_button_title" = "Retry";
"MJCS_passive_liveness_completed_button_title" = "Continue";

"MJCS_passive_liveness_upload_error_title" = "Uh Oh";
"MJCS_passive_liveness_upload_error_face_too_close_message" = "You’re too close. Move away so that your face fits inside the frame";
"MJCS_passive_liveness_upload_error_face_close_border_message" = "You’re too close. Move away so that your face fits inside the frame";
"MJCS_passive_liveness_upload_error_face_cropped_message" = "Something went wrong, please try again";
"MJCS_passive_liveness_upload_error_face_not_found_message" = "We’re unable to detect your face, please try again";
"MJCS_passive_liveness_upload_error_face_too_small_message" = "You are too far away. Move closer so that your face fits inside the frame";
"MJCS_passive_liveness_upload_error_face_angle_too_large_message" = "Something went wrong, please try again";
"MJCS_passive_liveness_upload_error_too_many_faces_message" = "We’re unable to detect your face, please try again";
"MJCS_passive_liveness_upload_error_unknown_message" = "Something went wrong, please try again";
"MJCS_passive_liveness_upload_error_internal_message" = "Something went wrong, please try again";

"MJCS_action_liveness_action_message" = "Keep your face inside the frame and look straight at the camera";
"MJCS_liveness_action_error_retry" = "and try again";
"MJCS_liveness_action_keep_going" = "Keep going";
"MJCS_liveness_action_hold_smile" = "Hold that smile";
"MJCS_liveness_action_hold_frown" = "Hold that frown";
"MJCS_action_liveness_error_title" = "Something went wrong!";
"MJCS_liveness_upload_loading_title" = "Just a moment…";
"MJCS_liveness_upload_loading_subtitle" = "Performing liveness checks, won't be long";
"MJCS_liveness_upload_loading_button_retry_text" = "Retry";
"MJCS_liveness_upload_error_title" = "Something went wrong!";
"MJCS_liveness_upload_error_subtitle" = "Please retry";

"MJCS_glare_detected_description" = "Move the document away from direct light sources";

"MJCS_auto_capture_active" = "AUTO CAPTURE - ON";

"MJCS_help_text_title" = "How to use Auto Capture";
"MJCS_help_text_back_title" = "How to take a photo of the BACK of your Identity Document";
"MJCS_help_text_description" = "Auto Capture will automatically take a photograph of your Identity Document when it has recognised that it is of good enough quality.";
"MJCS_help_text_sub_title" = "Tips for good quality captures";
"MJCS_help_text_glare_title" = "Prevent Glare";
"MJCS_help_text_glare_description" = "Move the document away from direct light sources.";
"MJCS_help_text_blur_title" = "Prevent Blur";
"MJCS_help_text_blur_description" = "Hold the document or device steady.";
"MJCS_help_text_distance_title" = "Document Too Far";
"MJCS_help_text_distance_description" = "Fit the document fully within the frame.";
"MJCS_help_text_manual_capture_title" = "Still struggling?";
"MJCS_help_switch_text" = "Turn on Manual Capture";
"MJCS_help_close_button_text" = "Close";
"MJCS_help_button_text" = "Help";

// Triple Scan
"MJCS_triple_scan_not_accepted_title" = "Validation failed - document not accepted.";
"MJCS_triple_scan_not_accepted_subtitle" = "Your Identity Document is not an accepted document type.\n\n\nPlease try again with a different document.";
"MJCS_triple_scan_not_supported_title" = "Validation failed - document unknown.";
"MJCS_triple_scan_not_supported_subtitle" = "Your Identity Document could not be classified or is not supported.\n\n\nPlease try again.";
"MJCS_triple_scan_refer_title" = "Validation failed.";
"MJCS_triple_scan_refer_subtitle" = "Your Identity Document failed the validation. \n\n\nPlease try again.";
"MJCS_triple_scan_expired_title" = "Validation failed - document expired.";
"MJCS_triple_scan_expired_subtitle" = "Your Identity Document is expired. \n\n\nPlease try again with a valid document.";
"MJCS_triple_scan_cancel_verification_button" = "End Identity Verification";
"MJCS_triple_scan_retry_button" = "Try Again";
"MJCS_triple_scan_attempts_remaining_text" = "You have %d attempt(s) remaining.";
"MJCS_triple_scan_quality_errors_text" = "Quality Issues Detected!";
"MJCS_triple_scan_quality_errors_blur_text" = "<b>• Blur</b> - the document is unclear;";
"MJCS_triple_scan_quality_errors_glare_text" = "<b>• Glare</b> - the document is unreadable due to a bright light source;";
"MJCS_triple_scan_quality_errors_low_res_text" = "<b>• Low Resolution</b> - the document does not fill the frame properly;";
"MJCS_triple_scan_quality_errors_boundary_text" = "<b>• Missing Edges</b> - the document is not positioned in the frame properly;";

"MJCS_manual_capture_label_text" = "Manual";
