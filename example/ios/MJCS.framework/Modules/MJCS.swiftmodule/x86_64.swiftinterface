// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target x86_64-apple-ios12.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MJCS
import AFNetworking
import AudioToolbox
import FLAnimatedImage
import Foundation
import Lottie
@_exported import MJCS
import MJCS.Private
import QuartzCore
import ReadID_UI
import Swift
import UIKit
@objc public protocol _Internal_IDSDocumentCameraViewControllerType {
  @objc var view: UIKit.UIView! { get set }
  @objc var additionalData: [Swift.String : Swift.AnyObject]? { get set }
  @objc var templateBottomText: Swift.String? { get set }
  @objc var isIdAnimationFinishedPlaying: Swift.Bool { get set }
  @objc var manualCaptureToggleEnabled: Swift.Bool { get set }
  @objc var manualCaptureToggleAppearTime: Foundation.TimeInterval { get set }
  @objc func playAnimationForIssueType(_ issueType: IDSCaptureIssuesViewType)
  @objc func didFinishPlayingAllQueuedAnimations()
  @objc func didDetectIssueType(_ issueType: IDSCaptureIssuesViewType)
  @objc func navigateToViewController(_ viewController: UIKit.UIViewController)
  @objc func playHelpIconAnimation()
  @objc func isHelpIconAnimationPlaying() -> Swift.Bool
  @objc func updateManualCaptureSwitchValue(_ isOn: Swift.Bool)
  @objc func updateUIForSuccessfulImageCaptured()
}
@objc public protocol _Internal_IDSDocumentCameraViewControllerDelegate : ObjectiveC.NSObjectProtocol {
  @objc func documentCameraDidDisableAutoScan(_ disabled: Swift.Bool)
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objcMembers final public class _Internal_IDSDocumentCameraViewController : IDSCameraController, _Internal_IDSDocumentCameraViewControllerType {
  @objc final public var issueVisibilityDuration: Swift.Double
  @objc final public var templateBottomText: Swift.String?
  @objc final public var additionalData: [Swift.String : Swift.AnyObject]?
  @objc final public var isIdAnimationFinishedPlaying: Swift.Bool
  @objc final public var manualCaptureToggleEnabled: Swift.Bool
  @objc final public var manualCaptureToggleAppearTime: Swift.Double
  @objc final public var documentCameraControllerDelegate: _Internal_IDSDocumentCameraViewControllerDelegate?
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc override final public func viewDidLoad()
  @objc override final public func viewDidLayoutSubviews()
  @objc override final public func viewWillAppear(_ animated: Swift.Bool)
  @objc override final public func viewDidAppear(_ animated: Swift.Bool)
  @objc override final public func hideCameraElements(_ hidden: Swift.Bool)
  @objc override final public func showAlertView(withInfo userInfo: [Swift.AnyHashable : Any])
  @objc override final public func updateTriggerButtonVisibility()
  @objc final public func updateManualCaptureSwitchValue(_ isOn: Swift.Bool)
  @objc final public func updateUIForSuccessfulImageCaptured()
  @objc final public func resetIssuesGuadanceUI()
  @objc deinit
}
extension _Internal_IDSDocumentCameraViewController {
  @objc final public func isHelpIconAnimationPlaying() -> Swift.Bool
  @objc final public func playHelpIconAnimation()
  @objc override final public func helpAction(_ button: UIKit.UIButton)
  @objc final public func navigateToViewController(_ viewController: UIKit.UIViewController)
}
extension _Internal_IDSDocumentCameraViewController {
  @objc final public func didDetectIssueType(_ issueType: IDSCaptureIssuesViewType)
  @objc final public func playAnimationForIssueType(_ issueType: IDSCaptureIssuesViewType)
  @objc final public func didFinishPlayingAllQueuedAnimations()
  @objc final public func updateIssueGuidanceUiIsError(_ isError: Swift.Bool)
}
@_inheritsConvenienceInitializers @IBDesignable @objc public class IDSSpinnerView : UIKit.UIView {
  @objc override dynamic public var layer: QuartzCore.CAShapeLayer {
    @objc get
  }
  @objc override dynamic public class var layerClass: Swift.AnyClass {
    @objc get
  }
  @IBInspectable @objc public var strokeColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc override dynamic public func layoutSubviews()
  @objc override dynamic public func didMoveToWindow()
  @objc override dynamic public func awakeFromNib()
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc public protocol IDSCaptureHelpViewControllerType {
  @objc func showManualCaptureToggleAnimated(_ animated: Swift.Bool)
  @objc func playAnimationForIssueView(atIndex index: Swift.Int)
  @objc var view: UIKit.UIView! { get set }
}
@objc @_inheritsConvenienceInitializers @objcMembers final public class IDSCaptureHelpViewController : UIKit.UIViewController, IDSCaptureHelpViewControllerType {
  @objc final public var presenter: IDSCaptureHelpPresenterType?
  @objc override final public var preferredStatusBarStyle: UIKit.UIStatusBarStyle {
    @objc get
  }
  @objc override final public func viewDidLoad()
  @objc override final public func viewDidAppear(_ animated: Swift.Bool)
  @objc final public func showManualCaptureToggleAnimated(_ animated: Swift.Bool)
  @objc final public func playAnimationForIssueView(atIndex index: Swift.Int)
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
public enum IDSError : Swift.Error, Foundation.LocalizedError {
  case invalidConfiguration(message: Swift.String)
  case NFCDeviceNotSupported
  public var failureReason: Swift.String {
    get
  }
}
@objc(IDSError) public enum _ObjCIDSError : Swift.Int {
  case invalidConfiguration
  case NFCDeviceNotSupported
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum IDSFailure : Swift.Error, Foundation.LocalizedError {
  case sessionTimeout
  case sessionError
  case connectionSecurityError
  case userCancelled
  case networkError(statusCode: Swift.Int, errorDescription: Swift.String)
  case internalFailure(reason: Swift.String, file: Swift.String = #file, function: Swift.String = #function, line: Swift.Int = #line)
  case nfcError
  public var failureReason: Swift.String? {
    get
  }
}
@_hasMissingDesignatedInitializers @objc(IDSFailure) @objcMembers final public class _ObjCIDSFailure : ObjectiveC.NSObject {
  @objc final public var reason: _ObjCIDSFailureReason {
    @objc get
  }
  @objc final public var error: Swift.Error? {
    @objc get
  }
  @objc override dynamic public init()
  @objc deinit
}
@objc(IDSFailureReason) public enum _ObjCIDSFailureReason : Swift.Int {
  case sessionTimeout
  case sessionError
  case connectionSecurityError
  case userCancelled
  case networkError
  case internalFailure
  case nfcError
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @objcMembers final public class IDSCaptureIssuesReplayingAnimationView : IDSCaptureIssuesView {
  @objc final public var animationPlayCount: Swift.Int
  @objc override final public func awakeFromNib()
  @objc override final public func playAnimation()
  @objc final public func activateReplayButton()
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc extension NSString {
  @objc dynamic public func localizedString() -> Swift.String
}
extension UILabel {
  @objc @IBInspectable dynamic public var xibLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
extension UIButton {
  @objc @IBInspectable dynamic public var xibLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
extension UINavigationItem {
  @objc @IBInspectable dynamic public var xibLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
extension UIBarItem {
  @objc @IBInspectable dynamic public var xibLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
extension UISegmentedControl {
  @objc @IBInspectable dynamic public var xibLocKeys: Swift.String? {
    @objc get
    @objc set(keys)
  }
}
extension UITextField {
  @objc @IBInspectable dynamic public var xibPlaceholderLocKey: Swift.String? {
    @objc get
    @objc set(key)
  }
}
@objc @objcMembers final public class IDSMetadataManager : ObjectiveC.NSObject {
  @objc public init(withStep step: IDSEnterpriseRequiredAction)
  @objc final public func resetMetadata(withNewStep step: IDSEnterpriseRequiredAction)
  @objc final public func addEntryWithKey(_ key: IDSMetadataKeys.IDSINTERNAL_METADATA_, value: Swift.String)
  @objc final public func addEntryWithKey(_ key: IDSMetadataKeys.IDSINTERNAL_METADATA_, valueConstant: IDSMetadataValue.IDSINTERNAL_METADATA_VALUE_)
  @objc final public func addComplexEntryWithKey(_ key: IDSMetadataKeys.IDSINTERNAL_METADATA_, value: Swift.AnyObject)
  @objc final public func getCurrentEntries() -> Swift.Set<IDSEnterpriseMetadataEntry>
  @objc final public func addBoundaryFrame()
  @objc final public func addGlareFame()
  @objc final public func addLowResFrame()
  @objc final public func addBlurFrame()
  @objc final public func addSmartCaptureFrameTime(_ time: Foundation.TimeInterval)
  @objc final public func recordMemoryFootprint()
  @objc override dynamic public init()
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSGlobalConfig : ObjectiveC.NSObject {
  @objc final public let appearance: MJCSAppearance
  @objc override dynamic public init()
  @objc public static func builder() -> IDSGlobalConfig
  @objc @discardableResult
  public func withCustomLocalization() -> IDSGlobalConfig
  @objc @discardableResult
  public func withCustomLocalization(tableName: Swift.String, inBundle: Foundation.Bundle?) -> IDSGlobalConfig
  @objc deinit
}
@objc public enum IDSDocumentScannerType : Swift.Int {
  case document = 0
  case utility = 1
  case selfie = 2
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSDocumentScannerImgQualityConfig : ObjectiveC.NSObject {
  @objc public var low_resolution_check_enabled: Swift.Bool {
    get
  }
  @objc public var blur_check_enabled: Swift.Bool {
    get
  }
  @objc public var glare_check_enabled: Swift.Bool {
    get
  }
  @objc public var boundary_check_enabled: Swift.Bool {
    get
  }
  @objc public static func builder() -> IDSDocumentScannerImgQualityConfig
  @objc @discardableResult
  public func withLowResolutionCheckEnabled(_ value: Swift.Bool) -> IDSDocumentScannerImgQualityConfig
  @objc @discardableResult
  public func withGlareCheckEnabled(_ value: Swift.Bool) -> IDSDocumentScannerImgQualityConfig
  @objc @discardableResult
  public func withBlurCheckEnabled(_ value: Swift.Bool) -> IDSDocumentScannerImgQualityConfig
  @objc @discardableResult
  public func withBoundaryCheckEnabled(_ value: Swift.Bool) -> IDSDocumentScannerImgQualityConfig
  @objc override dynamic public init()
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSDocumentScannerConfig : ObjectiveC.NSObject {
  @objc public var manualCaptureToggleEnabled: Swift.Bool {
    get
  }
  @objc public var enable4k: Swift.Bool {
    get
  }
  @objc public var scannerType: IDSDocumentScannerType {
    get
  }
  @objc public var cropDocumentImages: Swift.Bool {
    get
  }
  @objc public var captureButtonAppearTime: Swift.Double {
    get
  }
  @objc public var manualCaptureToggleAppearTime: Swift.Double {
    get
  }
  @objc public var ImgQualityConfig: IDSDocumentScannerImgQualityConfig {
    get
  }
  @objc public static func builder() -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withManualCaptureToggleEnabled(_ value: Swift.Bool) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func with4KCaptureEnabled(_ value: Swift.Bool) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withScannerType(_ value: IDSDocumentScannerType) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withUsingCroppedDocumentImages(_ value: Swift.Bool) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withCaptureButtonAppearTime(_ value: Swift.Double) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withManualCaptureToggleAppearTime(_ value: Swift.Double) -> IDSDocumentScannerConfig
  @objc @discardableResult
  public func withImgQualityConfig(_ value: IDSDocumentScannerImgQualityConfig) -> IDSDocumentScannerConfig
  @objc override dynamic public init()
  @objc deinit
}
extension UIView {
  @discardableResult
  public func fillSuperView(_ edges: UIKit.UIEdgeInsets = UIEdgeInsets.zero) -> [UIKit.NSLayoutConstraint]
  @discardableResult
  public func addLeadingConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .leading, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addTrailingConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .trailing, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addLeftConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .left, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addRightConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .right, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addTopConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .top, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addBottomConstraint(toView view: UIKit.UIView?, attribute: UIKit.NSLayoutConstraint.Attribute = .bottom, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addCenterXConstraint(toView view: UIKit.UIView?, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addCenterYConstraint(toView view: UIKit.UIView?, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addWidthConstraint(toView view: UIKit.UIView?, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @discardableResult
  public func addHeightConstraint(toView view: UIKit.UIView?, relation: UIKit.NSLayoutConstraint.Relation = .equal, constant: CoreGraphics.CGFloat = 0.0) -> UIKit.NSLayoutConstraint
  @objc dynamic public func setupEdgeToEdgeConstraintsInSuperview()
  @objc dynamic public func setupEdgeToEdgeConstraintsInSuperview(withInsets insets: UIKit.UIEdgeInsets)
}
public typealias IDSLottieAnimationCompletionBlock = (Swift.Bool) -> Swift.Void
@objc public enum IDSCaptureIssuesViewType : Swift.Int {
  case edge = 0
  case lowRes = 1
  case blur = 2
  case glare = 3
  case none = -1
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSCaptureIssuesViewBase : UIKit.UIView {
  @objc @IBOutlet weak public var animationsSuperview: UIKit.UIView!
  @objc public var issueViewType: IDSCaptureIssuesViewType
  @objc public var animationView: IDSLottieAnimationView?
  @objc public var animationCompletionBlock: IDSLottieAnimationCompletionBlock?
  @objc public var titleLabelFont: UIKit.UIFont? {
    @objc get
    @objc set
  }
  @objc public var descriptionLabelFont: UIKit.UIFont? {
    @objc get
    @objc set
  }
  @objc public var titleLabelTextColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc public var descriptionLabelTextColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @objc override dynamic public func awakeFromNib()
  @objc public func updateLabelsText()
  @objc public func updateIssueViewTypeTo(_ issueViewType: IDSCaptureIssuesViewType)
  @objc public func updateAnimationView()
  @objc public func playAnimation()
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
extension IDSCaptureIssuesViewBase {
  @objc dynamic public func titleText() -> Swift.String
  @objc dynamic public func descriptionText() -> Swift.String
}
@objc @_inheritsConvenienceInitializers @objcMembers final public class _Internal_IDSDocumentCameraPresenter : ObjectiveC.NSObject {
  @objc weak final public var viewController: _Internal_IDSDocumentCameraViewControllerType?
  @objc final public var issueVisibilityDuration: Foundation.TimeInterval
  @objc final public var manualCaptureToggleEnabled: Swift.Bool
  @objc final public var manualCaptureToggleAppearTime: Foundation.TimeInterval
  @objc final public func didFinishPlayingIdAnimation()
  @objc final public func didTapHelpButton(isManualCaptureToggleOn: Swift.Bool)
  @objc override dynamic public init()
  @objc deinit
}
extension _Internal_IDSDocumentCameraPresenter {
  @objc final public func successfulImageDidCapture()
  @objc final public func manualCaptureAppearDelayExpired()
  @objc final public func didDetectIssueType(_ issueType: IDSCaptureIssuesViewType)
  @objc final public func proceedToPlayNextAnimationIfNeeded()
}
@objc extension IDSCustomerJourneyController {
  @objc dynamic public func tripleScanVC(_ enterpriseResponse: IDSEnterpriseResponse) -> IDSCustomerJourneyUIController?
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSCaptureIssuesView : IDSCaptureIssuesViewBase {
  @objc override dynamic public func titleText() -> Swift.String
  @objc override dynamic public func descriptionText() -> Swift.String
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @objcMembers final public class IDSUtilityFunctions : ObjectiveC.NSObject {
  @objc final public class func authoriseCameraUsage(_ authorizationGranted: @escaping ((Swift.Bool) -> Swift.Void))
  @objc override dynamic public init()
  @objc deinit
}
public enum IDSNetworkingError : Swift.Int {
  case unknown
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
extension Error {
  public func getNetworkingErrorDesc() -> Swift.String
  public func getNetwrokingErrorCode() -> Swift.Int
  public func getNormalisedError() -> Swift.Error
}
@objc extension NSError {
  @objc dynamic public func getNetworkingErrorDesc() -> Swift.String
  @objc dynamic public func getNetwrokingErrorCode() -> Swift.Int
  @objc dynamic public func getNormalisedError() -> Swift.Error
}
@objc public enum _InternalButtonUIState : Swift.Int {
  case unknown = 0
  case primary
  case destructive
  case tertiary
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @objcMembers public class _InternalUIButton : UIKit.UIButton {
  @objc public func updateUIState(_ newState: _InternalButtonUIState)
  @objc override dynamic public func touchesBegan(_ touches: Swift.Set<UIKit.UITouch>, with event: UIKit.UIEvent?)
  @objc override dynamic public func touchesEnded(_ touches: Swift.Set<UIKit.UITouch>, with event: UIKit.UIEvent?)
  @objc override dynamic public func touchesCancelled(_ touches: Swift.Set<UIKit.UITouch>, with event: UIKit.UIEvent?)
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @objcMembers final public class IDSEnterpriseRequestModifier : ObjectiveC.NSObject {
  @objc final public var additionalData: [IDSEnterpriseAdditionalDataEntry] {
    get
  }
  @objc override dynamic public init()
  @objc final public func addAdditionalData(_ data: Swift.Array<IDSEnterpriseAdditionalDataEntry>)
  @objc deinit
}
extension NSAttributedString {
  convenience public init?(htmlString: Swift.String)
}
extension NSMutableAttributedString {
  public func setBaseFont(baseFont: UIKit.UIFont, preserveFontSizes: Swift.Bool = false)
  public func setTextColour(_ textColor: UIKit.UIColor)
  public func setFont(_ font: UIKit.UIFont)
  public func setTextAlignment(_ textAlignment: UIKit.NSTextAlignment)
}
@objc @_inheritsConvenienceInitializers @objcMembers final public class MJCSAppearance : ObjectiveC.NSObject {
  @objc final public var destructiveBtnBorderColor: UIKit.UIColor
  @objc final public var destructiveBtnColor: UIKit.UIColor
  @objc final public var destructiveBtnPressedBorderColor: UIKit.UIColor
  @objc final public var destructiveBtnPressedColor: UIKit.UIColor
  @objc final public var destructiveBtnPressedTextColor: UIKit.UIColor
  @objc final public var destructiveBtnTextColor: UIKit.UIColor
  @objc final public var heading1Color: UIKit.UIColor
  @objc final public var heading2Color: UIKit.UIColor
  @objc final public var heading3Color: UIKit.UIColor
  @objc final public var paragraph1Color: UIKit.UIColor
  @objc final public var paragraph2Color: UIKit.UIColor
  @objc final public var primaryBGColor: UIKit.UIColor
  @objc final public var primaryBtnBorderColor: UIKit.UIColor
  @objc final public var primaryBtnColor: UIKit.UIColor
  @objc final public var primaryBtnPressedBorderColor: UIKit.UIColor
  @objc final public var primaryBtnPressedColor: UIKit.UIColor
  @objc final public var primaryBtnPressedTextColor: UIKit.UIColor
  @objc final public var primaryBtnTextColor: UIKit.UIColor
  @objc final public var tertiaryIconBtnColor: UIKit.UIColor
  @objc final public var tertiaryIconBtnPressedColor: UIKit.UIColor
  @objc final public var primaryBtnBorderWidth: CoreGraphics.CGFloat
  @objc final public var primaryBtnCornerRadius: CoreGraphics.CGFloat
  @objc final public var destructiveBtnBorderWidth: CoreGraphics.CGFloat
  @objc final public var destructiveBtnCornerRadius: CoreGraphics.CGFloat
  @objc final public var heading1FontName: Swift.String
  @objc final public var heading2FontName: Swift.String
  @objc final public var heading3FontName: Swift.String
  @objc final public var paragraph1FontName: Swift.String
  @objc final public var paragraph2FontName: Swift.String
  @objc final public var primaryButtonFontName: Swift.String
  @objc final public var destructiveButtonFontName: Swift.String
  @objc final public var tertiaryButtonFontName: Swift.String
  @objc final public var heading1FontSize: CoreGraphics.CGFloat
  @objc final public var heading2FontSize: CoreGraphics.CGFloat
  @objc final public var heading3FontSize: CoreGraphics.CGFloat
  @objc final public var paragraph1FontSize: CoreGraphics.CGFloat
  @objc final public var paragraph2FontSize: CoreGraphics.CGFloat
  @objc final public var primaryButtonFontSize: CoreGraphics.CGFloat
  @objc final public var destructiveButtonFontSize: CoreGraphics.CGFloat
  @objc final public var tertiaryButtonFontSize: CoreGraphics.CGFloat
  @objc override dynamic public init()
  @objc deinit
}
extension MJCSAppearance {
  @objc final public func _internalHeading1Font() -> UIKit.UIFont
  @objc final public func _internalHeading2Font() -> UIKit.UIFont
  @objc final public func _internalHeading3Font() -> UIKit.UIFont
  @objc final public func _internalParagraph1Font() -> UIKit.UIFont
  @objc final public func _internalParagraph2Font() -> UIKit.UIFont
  @objc final public func _internalPrimaryButtonFont() -> UIKit.UIFont
  @objc final public func _internalDestructiveButtonFont() -> UIKit.UIFont
  @objc final public func _internalTertiaryButtonFont() -> UIKit.UIFont
}
@objc public protocol IDSLivenessOverlayViewControllerProtocol {
  @objc func readyForLiveness()
  @objc func userCancelled()
}
@_inheritsConvenienceInitializers @objc(IDSLivenessOverlayViewController) public class IDSLivenessOverlayViewController : UIKit.UIViewController {
  @objc weak public var delegate: IDSLivenessOverlayViewControllerProtocol?
  @objc override dynamic public func viewDidLoad()
  @objc override dynamic public func viewDidLayoutSubviews()
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
extension IDSLivenessOverlayViewController : IDSLivenessViewProtocol {
  @objc dynamic public func requestedCalibration(_ calibration: IDSLivenessCalibration)
  @objc dynamic public func livenessLoadingFinished()
  @objc dynamic public func livenessFinished()
  @objc dynamic public func requestedAction(_ action: Swift.Int)
}
@objc @objcMembers public class IDSLivenessSubmissionHandler : ObjectiveC.NSObject {
  public typealias ResponseHandler = (Swift.Error?, IDSEnterpriseResponse?) -> Swift.Void
  @objc public init(credentials: IDSEnterpriseCredentials, journeyID: Swift.String, responseHandler: @escaping IDSLivenessSubmissionHandler.ResponseHandler)
  @objc public func onFace(image: UIKit.UIImage)
  @objc public func onLiveness(result: Any)
  @objc override dynamic public init()
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSMetadataKeys : ObjectiveC.NSObject {
  @objc public enum IDSINTERNAL_METADATA_ : Swift.Int {
    case SCANNER_DocType
    case SCANNER_Profile
    case SCANNER_SmartCaptureTime
    case SCANNER_IDESProcessTime
    case SCANNER_IDESProcessTimeAvg
    case SCANNER_BlurryFrameCount
    case SCANNER_GlaryFrameCount
    case SCANNER_LowResFrameCount
    case SCANNER_BoundaryFrameCount
    case SCANNER_CaptureMethod
    case SCANNER_SmartCaptureFrameCount
    case SCANNER_MemoryAvg
    case SCANNER_MemoryMax
    case CONFIG_GlareCheckEnabled
    case CONFIG_BlurCheckEnabled
    case CONFIG_LowResCheckEnabled
    case CONFIG_BoundaryCheckEnabled
    case CONFIG_ManualCaptureDisabled
    case CONFIG_ManualCaptureAppearTime
    case CONFIG_CropDocumentImages
    case CONFIG_ManualCaptureToggleEnabled
    case LIVENESS_ScanTime
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  @objc public class func string(_ keyName: IDSMetadataKeys.IDSINTERNAL_METADATA_) -> Swift.String
  @objc override dynamic public init()
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSMetadataValue : ObjectiveC.NSObject {
  @objc public enum IDSINTERNAL_METADATA_VALUE_ : Swift.Int {
    case SCANNER_CaptureMethod_SmartCapture
    case SCANNER_CaptureMethod_ManualCapture
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  @objc public class func string(_ valueName: IDSMetadataValue.IDSINTERNAL_METADATA_VALUE_) -> Swift.String
  @objc override dynamic public init()
  @objc deinit
}
@objc @_hasMissingDesignatedInitializers @objcMembers public class IDSNFCScannerViewController : UIKit.UIViewController {
  public enum IDSNFCResult {
    case success(nextAction: IDSEnterpriseRequiredAction)
    case failed(reason: IDSFailure, nextAction: IDSEnterpriseRequiredAction)
  }
  @objc(IDSNFCResultType) public enum _ObjCIDSNFCResultType : Swift.Int {
    case success
    case failed
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
  @_hasMissingDesignatedInitializers @objc(IDSNFCResult) @objcMembers final public class _ObjCIDSNFCResult : ObjectiveC.NSObject {
    @objc final public var resultState: IDSNFCScannerViewController._ObjCIDSNFCResultType {
      @objc get
    }
    @objc final public var result: Any? {
      @objc get
    }
    @objc override dynamic public init()
    @objc deinit
  }
  @_hasMissingDesignatedInitializers @objc(IDSNFCResultSuccess) @objcMembers final public class _ObjCIDSNFCResultSuccess : ObjectiveC.NSObject {
    @objc final public var nextAction: IDSEnterpriseRequiredAction {
      get
    }
    @objc override dynamic public init()
    @objc deinit
  }
  @_hasMissingDesignatedInitializers @objc(IDSNFCResultFailure) @objcMembers final public class _ObjCIDSNFCResultFailure : ObjectiveC.NSObject {
    @objc final public var nextAction: IDSEnterpriseRequiredAction {
      get
    }
    @objc final public var failure: _ObjCIDSFailure {
      get
    }
    @objc override dynamic public init()
    @objc deinit
  }
  public typealias CompletionHandler = (IDSNFCScannerViewController.IDSNFCResult) -> ()
  public init(credentials: IDSEnterpriseCredentials, lastStepResponse: IDSEnterpriseResponse, completionHandler: @escaping IDSNFCScannerViewController.CompletionHandler) throws
  @objc @available(swift, obsoleted: 1.0)
  convenience public init(credentials: IDSEnterpriseCredentials, lastStepResponse: IDSEnterpriseResponse, completionHandler: @escaping (IDSNFCScannerViewController._ObjCIDSNFCResult) -> ()) throws
  @objc override dynamic public func viewDidLoad()
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc deinit
}
public typealias _Internal_ButtonTapActionHandler = (_Internal_IDSLivenessFeedbackViewControllerState) -> Swift.Void
public protocol _Internal_IDSLivenessFeedbackViewControllerType : AnyObject {
  var state: _Internal_IDSLivenessFeedbackViewControllerState { get set }
  var subtitleLabelText: Swift.String? { get set }
  var buttonTapActionHandler: _Internal_ButtonTapActionHandler? { get set }
  func updateUI()
  func doActionOnButtonTap()
  func doFinishAction()
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objcMembers final public class _Internal_IDSLivenessFeedbackViewController : IDSCustomerJourneyUIController, _Internal_IDSLivenessFeedbackViewControllerType {
  @objc final public var buttonTapActionHandler: _Internal_ButtonTapActionHandler?
  @objc final public var onFinish: (() -> (Swift.Void))?
  @objc final public var onCancel: (() -> (Swift.Void))?
  @objc final public var presenter: _Internal_IDSLivenessFeedbackPresenter?
  @objc final public var state: _Internal_IDSLivenessFeedbackViewControllerState {
    @objc get
    @objc set
  }
  @objc final public var subtitleLabelText: Swift.String? {
    @objc get
    @objc set
  }
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc override final public func viewDidLoad()
  @objc deinit
}
extension _Internal_IDSLivenessFeedbackViewController {
  @objc final public func updateUI()
}
extension _Internal_IDSLivenessFeedbackViewController {
  @objc final public func doActionOnButtonTap()
  @objc final public func doFinishAction()
}
extension _Internal_IDSLivenessFeedbackViewController : IDSCustomerJourneyUIUploadingProtocol {
  @objc final public func onUploadingError(_ error: Swift.Error)
  @objc final public func onUploadingFinished()
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objcMembers final public class MJCSToggleSwitch : UIKit.UIView {
  @objc final public var scale: CoreGraphics.CGFloat
  @objc final public var isOn: Swift.Bool
  @objc final public var switchBorderColor: UIKit.UIColor
  @objc final public var switchBorderWidth: CoreGraphics.CGFloat
  @objc final public var onStateTintColor: UIKit.UIColor
  @objc final public var onStateThumbTintColor: UIKit.UIColor
  @objc final public var offStateThumbTintColor: UIKit.UIColor
  @objc final public var tickIconColorColor: UIKit.UIColor
  @objc final public var onValueChange: ((Swift.Bool) -> ())?
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc override final public func didMoveToWindow()
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSCustomerJourneyConfig : ObjectiveC.NSObject {
  @objc public var documentScannerConfig: IDSDocumentScannerConfig {
    get
  }
  @objc public static func builder() -> IDSCustomerJourneyConfig
  @objc @discardableResult
  public func withDocumentScannerConfig(_ value: IDSDocumentScannerConfig) -> IDSCustomerJourneyConfig
  @objc override dynamic public init()
  @objc deinit
}
@objc public protocol IDSCaptureHelpPresenterType {
  @objc var onManualCaptureToggleValueChanged: ((Swift.Bool) -> Swift.Void)? { get set }
  @objc var hasManualCaptureAppearDelayExpired: Swift.Bool { get set }
  @objc var isManualCaptureToggleOn: Swift.Bool { get set }
  @objc var issueViewsArray: [IDSCaptureIssuesReplayingAnimationView] { get set }
  @objc func titleText() -> Swift.String
  @objc func descriptionText() -> Swift.String
  @objc func guidanceSectionTitleText() -> Swift.String
  @objc func closeButtonTitleText() -> Swift.String
  @objc func bottomViewTitleText() -> Swift.String
  @objc func bottomViewMessageText() -> Swift.String
  @objc func manualCaptureAppearTimerExpired()
  @objc func manualCaptureToggleValueDidChange(isOn: Swift.Bool)
  @objc func setupCaptureIssueViewArray()
  @objc func viewDidAppear()
}
@objc @objcMembers final public class IDSCaptureHelpPresenter : ObjectiveC.NSObject, IDSCaptureHelpPresenterType {
  @objc unowned final public let viewController: IDSCaptureHelpViewControllerType
  @objc final public var onManualCaptureToggleValueChanged: ((Swift.Bool) -> Swift.Void)?
  @objc final public var hasManualCaptureAppearDelayExpired: Swift.Bool
  @objc final public var isManualCaptureToggleOn: Swift.Bool
  @objc final public var issueViewsArray: [IDSCaptureIssuesReplayingAnimationView]
  @objc public init(viewController: IDSCaptureHelpViewControllerType, screenTexts: IDSCaptureHelpScreenTextsType)
  @objc final public func setupCaptureIssueViewArray()
  @objc final public func viewDidAppear()
  @objc final public func titleText() -> Swift.String
  @objc final public func descriptionText() -> Swift.String
  @objc final public func guidanceSectionTitleText() -> Swift.String
  @objc final public func closeButtonTitleText() -> Swift.String
  @objc final public func bottomViewTitleText() -> Swift.String
  @objc final public func bottomViewMessageText() -> Swift.String
  @objc final public func manualCaptureAppearTimerExpired()
  @objc final public func manualCaptureToggleValueDidChange(isOn: Swift.Bool)
  @objc override dynamic public init()
  @objc deinit
}
@objc public protocol IDSCaptureHelpScreenTextsType {
  @objc var titleKey: Swift.String { get set }
  @objc var helpDescriptionKey: Swift.String { get }
  @objc var guidanceSectionTitleKey: Swift.String { get }
  @objc var closeButtonTitleKey: Swift.String { get }
  @objc var bottomViewTitleKey: Swift.String { get }
  @objc var bottomViewMessageKey: Swift.String { get }
}
@objc @_inheritsConvenienceInitializers @objcMembers public class IDSCaptureHelpScreenTexts : ObjectiveC.NSObject, IDSCaptureHelpScreenTextsType {
  @objc public var titleKey: Swift.String
  @objc final public let helpDescriptionKey: Swift.String
  @objc final public let guidanceSectionTitleKey: Swift.String
  @objc final public let closeButtonTitleKey: Swift.String
  @objc final public let bottomViewTitleKey: Swift.String
  @objc final public let bottomViewMessageKey: Swift.String
  @objc override dynamic public init()
  @objc deinit
}
@objc public enum _Internal_IDSLivenessFeedbackViewControllerState : Swift.Int {
  case loading
  case error
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @objcMembers final public class _Internal_IDSLivenessFeedbackPresenter : ObjectiveC.NSObject {
  weak final public var viewController: _Internal_IDSLivenessFeedbackViewControllerType?
  @objc final public var subtitleLabelText: Swift.String
  @objc final public func titleLabelText() -> Swift.String
  @objc final public func bottomButtonTitleText() -> Swift.String
  @objc final public func isBottomButtonHidden() -> Swift.Bool
  @objc final public func bottomButtonTapped()
  @objc final public func uploadingDidFinish()
  @objc final public func uploadingDidFail(withError error: Foundation.NSError)
  @objc override dynamic public init()
  @objc deinit
}
extension _ObjCIDSError : Swift.Equatable {}
extension _ObjCIDSError : Swift.Hashable {}
extension _ObjCIDSError : Swift.RawRepresentable {}
extension _ObjCIDSFailureReason : Swift.Equatable {}
extension _ObjCIDSFailureReason : Swift.Hashable {}
extension _ObjCIDSFailureReason : Swift.RawRepresentable {}
extension IDSDocumentScannerType : Swift.Equatable {}
extension IDSDocumentScannerType : Swift.Hashable {}
extension IDSDocumentScannerType : Swift.RawRepresentable {}
extension IDSCaptureIssuesViewType : Swift.Equatable {}
extension IDSCaptureIssuesViewType : Swift.Hashable {}
extension IDSCaptureIssuesViewType : Swift.RawRepresentable {}
extension IDSNetworkingError : Swift.Equatable {}
extension IDSNetworkingError : Swift.Hashable {}
extension IDSNetworkingError : Swift.RawRepresentable {}
extension _InternalButtonUIState : Swift.Equatable {}
extension _InternalButtonUIState : Swift.Hashable {}
extension _InternalButtonUIState : Swift.RawRepresentable {}
extension IDSMetadataKeys.IDSINTERNAL_METADATA_ : Swift.Equatable {}
extension IDSMetadataKeys.IDSINTERNAL_METADATA_ : Swift.Hashable {}
extension IDSMetadataKeys.IDSINTERNAL_METADATA_ : Swift.RawRepresentable {}
extension IDSMetadataValue.IDSINTERNAL_METADATA_VALUE_ : Swift.Equatable {}
extension IDSMetadataValue.IDSINTERNAL_METADATA_VALUE_ : Swift.Hashable {}
extension IDSMetadataValue.IDSINTERNAL_METADATA_VALUE_ : Swift.RawRepresentable {}
extension IDSNFCScannerViewController._ObjCIDSNFCResultType : Swift.Equatable {}
extension IDSNFCScannerViewController._ObjCIDSNFCResultType : Swift.Hashable {}
extension IDSNFCScannerViewController._ObjCIDSNFCResultType : Swift.RawRepresentable {}
extension _Internal_IDSLivenessFeedbackViewControllerState : Swift.Equatable {}
extension _Internal_IDSLivenessFeedbackViewControllerState : Swift.Hashable {}
extension _Internal_IDSLivenessFeedbackViewControllerState : Swift.RawRepresentable {}
