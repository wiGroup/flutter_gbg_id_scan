//
//  IDSEnterpriseMetadataEntry+Private.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import "IDSEnterpriseMetadataEntry.h"

NS_ASSUME_NONNULL_BEGIN

// Key values for metadata network requests as required by IEOS
extern NSString *const kIDSMetadataEntryNameKey;
extern NSString *const kIDSMetadataEntryValueKey;
extern NSString *const kIDSMetadataEntryStepKey;
extern NSString *const kIDSMetadataEntryTypeKey;

@interface IDSEnterpriseMetadataEntry (Private)
+ (instancetype)entryWithName:(NSString *_Nonnull)name
                       value:(NSString *_Nonnull)value
                        step:(IDSEnterpriseRequiredAction)step
                        type:(IDSEnterpriseMetadataEntryType)type;

/// Get mapped dictionary to send to IEOS as a request
- (NSDictionary*)getMappedDictionary;

/// Get string value for the step as IEOS would expect it
- (NSString*)getStepString;

/// Get entry type string as IEOS would expect it
- (NSString*)getTypeString;

@end

NS_ASSUME_NONNULL_END
