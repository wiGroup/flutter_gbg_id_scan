//
//  IDSEnterpriseMetadata+Private.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import "IDSEnterpriseMetadata.h"

NS_ASSUME_NONNULL_BEGIN

extern NSString *const kNotAvailableValue;
extern NSString *const kConnectivityWIFIValue;
extern NSString *const kConnectivity4GValue;
extern NSString *const kConnectivity3GValue;
extern NSString *const kConnectivityEDGEValue;
extern NSString *const kConnectivityGPRSValue;

@interface IDSEnterpriseMetadata (Private)

/// Get shared object
+ (IDSEnterpriseMetadata*)shared;

/// Set general metadata such as versions of SDK, phone etc
- (void)setGeneralMetadata;

@end

NS_ASSUME_NONNULL_END
