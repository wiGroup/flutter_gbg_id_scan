//
//  IDSEnterpriseMetadata.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDSEnterpriseMetadataEntry.h"
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, IDSEnterpriseMetadataSourceType) {
    IDSEnterpriseMetadataSourceTypeScanner = 0,
    IDSEnterpriseMetadataSourceTypeCustomerJourney
};

@interface IDSEnterpriseMetadata : NSObject

/// Who is currently adding metadata info into the object
@property (assign, readonly) IDSEnterpriseMetadataSourceType sourceType;

/// Metadata entries
@property (nonatomic, nonnull, readonly) NSMutableSet<IDSEnterpriseMetadataEntry*> *metadataSet;

/// Empty metadata
- (void)resetMetadata;

/// Get dictionary for network request as required by IEOS
- (NSArray*)getMappedDictionary;

/// Add entry of metadata
/// @param entry of metadata
- (void)addMetadataEntry:(IDSEnterpriseMetadataEntry*)entry;

/// Add multiple entries of metadata
/// @param entries array
- (void)addMetadataEntries:(NSArray <IDSEnterpriseMetadataEntry*> *)entries;

/// Set current source of metadata
/// @param sourceType current source of metadata
- (void)setMetadataSource:(IDSEnterpriseMetadataSourceType)sourceType;

@end

NS_ASSUME_NONNULL_END
