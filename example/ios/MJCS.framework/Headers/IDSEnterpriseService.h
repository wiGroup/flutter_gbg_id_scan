//
//  IDSEnterpriseService.h
//  IDSWebServices
//
//  Created by David Okun on 15/09/2016.
//  Copyright © 2016 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>
#import "IDSEnterpriseResponse.h"
#import "IDSEnterpriseSendRequest.h"
#import "IDSEnterpriseJourneyResponse.h"
#import "IDSEnterpriseRequestLiveness.h"
#import "IDSEnterpriseJourneyDefinition.h"
#import "IDSEnterpriseRequestSkipNFC.h"
#import "IDSEnterprisePassiveLivenessRequest.h"

/**
 Service class to communicate images back and forth between the instance of an IDscan Enterprise Server
 */
@interface IDSEnterpriseService : NSObject

/**
 Method that allows the user to submit an identity document to the enterprise server with the ability to construct request with custom data


 @param constructingBlock A block that takes a single argument and appends custom data for sending request.
 @param progressHandler Asynchronous block that returns upload progress of the document being uploaded for UI purposes
 @param completionHandler Asynchronous block that returns either the response of the server, or an error
 */
+ (void)submitDocumentConstructingWithBlock:(void (^_Nonnull)(IDSEnterpriseSendRequest *_Nonnull request))constructingBlock
                                   progress:(void (^_Nullable)(NSProgress *_Nonnull uploadProgress))progressHandler
                                 completion:(void (^_Nonnull)(IDSEnterpriseResponse *_Nullable response, NSError *_Nullable error))completionHandler;

/**
 Method that allows the user to get further details on a scanned journey. This method could return metadata for multiple image scans, but all related to one specific scan referenced journey.
 
 @param journeyGUID The scan reference number that you want to search by.
 @param credentials depicting credentials to override for authentication.
 @param progressHandler   Asynchronous block that returns download progress for the request being made.
 @param completionHandler Asynchronous block that returns either the response of the server, or an error
 */
+ (void)getJourney:(nonnull NSString *)journeyGUID
          credentials:(nonnull IDSEnterpriseCredentials *)credentials
          progress:(void (^_Nullable)(NSProgress *_Nonnull uploadProgress))progressHandler
        completion:(void (^_Nonnull)(IDSEnterpriseJourneyResponse *_Nullable response, NSError *_Nullable error))completionHandler;

/**
 Method that allows the user to submit liveness to enterprise server
 
 @param livenessResult depicting liveness results comes with wrapper
 @param credentials depicting credentials to override for authentication.
 @param progressHandler   Asynchronous block that returns download progress for the request being made.
 @param completionHandler Asynchronous block that returns either the response of the server, or an error
 */
+ (void)submitLivenessResult:(nonnull IDSEnterpriseRequestLiveness *)livenessResult
                 credentials:(nonnull IDSEnterpriseCredentials *)credentials
                    progress:(void (^_Nullable)(NSProgress *_Nonnull uploadProgress))progressHandler
                  completion:(void (^_Nonnull)(IDSEnterpriseResponse *_Nullable response, NSError *_Nullable error))completionHandler;

/**
 Method that sends passive liveness to backend with the ability to construct request with custom data

 @param constructingBlock A block that takes a single argument and appends custom data for sending request.
 @param completionHandler Asynchronous block that returns either the response of the server, or an error
 */
+ (void)submitPassiveLivenessWithConstructingBlock:(void (^_Nonnull)(IDSEnterprisePassiveLivenessRequest *_Nonnull request))constructingBlock
                                        completion:(void (^_Nonnull)(IDSEnterpriseResponse *_Nullable response, NSError *_Nullable error))completionHandler;

/**
 Method that allows to receive journey definitions for mobile channel

 @param credentials depicting credentials to override for authentication.
 @param completionHandler Asynchronous block that returns either the response of the server, or an error
 */
+ (void)getJourneyDefinitionsWithCredentials:(nonnull IDSEnterpriseCredentials *)credentials
                                  completion:(void (^_Nonnull)(NSArray <IDSEnterpriseJourneyDefinition *> *_Nullable journeyDefinitions, NSError *_Nullable error))completionHandler;

/// Method that sends instruction to backend to skip the NFC
/// @param constructingBlock A block that takes a single argument and appends custom data for sending request.
/// @param completionHandler Asynchronous block that returns either the response of the server, or an error
+ (void)skipNFCConstructingWithBlock:(void (^_Nonnull)(IDSEnterpriseRequestSkipNFC *_Nonnull request))constructingBlock
                          completion:(void (^_Nullable)(BOOL success, NSError *_Nullable error) )completionHandler;
@end
