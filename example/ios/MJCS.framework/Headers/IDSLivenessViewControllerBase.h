//
//  IDSLivenessViewControllerBase.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 27/04/2020.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJCS/IDSEnterpriseCredentials.h>
#import <MJCS/IDSLivenessCalibration.h>
#import <MJCS/IDSLivenessProtocols.h>

@class IDSLivenessActionState;
@class IDSLivenessConfig;

NS_ASSUME_NONNULL_BEGIN
/**
 Protocol for livenessViewController overlays
 where different actions should be presented for end-user
 */
@protocol IDSLivenessViewProtocol <NSObject>

@required

/// Func returning liveness required action, which needs to be performed by the end user
/// @param action : required end-user action, please check IDSLivenessAction for values
- (void)requestedAction:(NSInteger)action;

@required

/// Func, which notifies about required calibration step for an end-user
 /// Usually, it occurs if a person holds phone too low, hence engine will ask to
 /// reposition a device and put it straight in front of the face
/// @param calibration struct, which holds information about required calibration action
- (void)requestedCalibration:(IDSLivenessCalibration*)calibration;

@optional
/// It takes some time to initiate liveness engine and
/// on different devices this time might vary. It is a good
/// practice to display a loader for end user and once engine is ready to be used
/// this func will be trigered
- (void)livenessLoadingFinished;

@optional
/// Func, which will be triggered once final result is available
- (void)livenessFinished;

@end

/// Internal
@interface IDSLivenessViewControllerBase : UIViewController

- (instancetype)initWithOverlay:(UIViewController <IDSLivenessViewProtocol> *)overlay
                         config:(nullable IDSLivenessConfig *)config journeyID:(NSString *)journeyID
                    credentials:(nullable IDSEnterpriseCredentials *)credentials
                         onlineProtocol:(id <IDSLivenessOnlineViewControllerProtocol>)onlineProtocol;

- (nonnull instancetype)initWithOverlay:(UIViewController <IDSLivenessViewProtocol>*)overlay
                                 config:(nullable IDSLivenessConfig *)config
                        offlineProtocol:(id <IDSLivenessOfflineViewControllerProtocol>)offlineProtocol;

- (void)startLiveness;
- (void)cancelLiveness;

@end
NS_ASSUME_NONNULL_END
