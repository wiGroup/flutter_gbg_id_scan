//
//  IDSOfflineLivenessViewController.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 27/04/2020.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJCS/IDSLivenessViewControllerBase.h>
#import <MJCS/IDSLivenessProtocols.h>

@class IDSLivenessViewControllerBase;
@class IDSLivenessOfflineViewController;
@class IDSLivenessResult;

NS_ASSUME_NONNULL_BEGIN

/// Liveness controller, which perform the scan without sending any requests to the backend.
/// Please be aware that it will only perform liveness scan without the face match, as this functionality requires backend connection (could be achieved using IDSLivenessOnlineViewController)
@interface IDSLivenessOfflineViewController : IDSLivenessViewControllerBase

/// Class constructor to initialise offline liveness detection with default overlay
/// @param delegate listener
/// @param configuration liveness engine configuration, if not provided, default will be used
- (nonnull instancetype)initWithDelegate:(id <IDSLivenessOfflineViewControllerProtocol> _Nonnull)delegate
                           configuration:(IDSLivenessConfig  * _Nullable )configuration;

/// Class constructor to initialise offline liveness detection with provided overlay
/// @param overlayController  overlay, which will be put on top of camera feed. Should adhere to described protocols
/// @param delegate listener
/// @param configuration liveness engine configuration, if not provided, default will be used
- (nonnull instancetype)initWithOverlayController:(UIViewController <IDSLivenessViewProtocol> * _Nonnull)overlayController
                                         delegate:(id <IDSLivenessOfflineViewControllerProtocol> _Nonnull)delegate
                                    configuration:(IDSLivenessConfig  * _Nullable)configuration;

@end

NS_ASSUME_NONNULL_END
