//
//  IDSLivenessCalibration.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 27/04/2020.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, IDSLivenessCalibrationDirection) {
    IDSLivenessCalibrationDirectionDown = 0,
    IDSLivenessCalibrationDirectionUp,
    IDSLivenessCalibrationDirectionLeft,
    IDSLivenessCalibrationDirectionRight
};

@interface IDSLivenessCalibration : NSObject

@property (nonatomic, assign) IDSLivenessCalibrationDirection moveDirection;

@property (nonatomic, assign) CGPoint facePose;

@property (nonatomic, assign) BOOL faceSeen;

@property (nonatomic, assign) BOOL poseError;

@property (nonatomic, nonnull) NSNumber* poseCounter;

@property (nonatomic, nonnull) NSNumber* poseCounterLimit;

@property (nonatomic, nonnull) NSNumber* actionTested;

@property (nonatomic, nonnull) NSNumber* passed;

@end

NS_ASSUME_NONNULL_END
