//
//  IDSCustomerJourneyController.h
//  MJCS
//
//  Created by Dicle Yilmaz on 22/11/2017.
//  Copyright © 2017 IDScan Biometrics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJCS/IDSEnterpriseCredentials.h>
#import <MJCS/IDSEnterpriseJourneyResponse.h>
#import <MJCS/IDSEnterpriseJourneyDefinition.h>
#import <MJCS/IDSEnterpriseRequiredActionState.h>
#import <MJCS/IDSCustomerJourneyUIController.h>

@class IDSCustomerJourneyController;
@class IDSCustomerJourneyConfig;
@class IDSEnterpriseRequestModifier;
@class IDSEnterpriseResponse;

@protocol IDSCustomerJourneyControllerDelegate <NSObject>

@optional
/**
 * Tells the delegate that the scanner is unable to perform its duties.
 */
- (void)customerJourneyController:(IDSCustomerJourneyController *_Nonnull)scanner didFailWithError:(NSError *_Nullable)error;

@optional
/**
 * Tells the delegate that the user cancelled the scan operation.
 */
- (void)customerJourneyControllerDidCancel:(IDSCustomerJourneyController *_Nonnull)scanner;

@optional
/**
 * Tells the delegate that the scanner recognized a document object
 * in the image.
 */
- (void)customerJourneyController:(IDSCustomerJourneyController *_Nonnull)scanner didFinishJourneyWithResult:(IDSEnterpriseJourneyResponse *_Nullable)result;

@optional
/**
 Asks method to return, which journey definition to use while performing a journey.
 If this method is not implemented either you can pass journeyDefinitionID directly to controller
 or IEOS backend will choose the default configuration

 @param scanner of customer journey
 @param journeyDefinitions Array of possible journey definitions that you need to choose from
 @return IDSEnterpriseJourneyDefinition which will be used for the journey
 */
- (IDSEnterpriseJourneyDefinition *_Nullable)customerJourneyController:(IDSCustomerJourneyController *_Nonnull)scanner selectJourneyDefinition:(NSArray <IDSEnterpriseJourneyDefinition *> *_Nullable)journeyDefinitions;

@optional
/// Asks method whether intermediate view controller between different steps should be presented during the journey
/// @param scanner of customer journey
/// @param nextStep the enum value of the next step that will be performed
/// @deprecated since 10.6.0, please use customerJourneyController:intermediateViewControllerForStep instead
- (IDSCustomerJourneyUIController <IDSCustomerJourneyUIUploadingProtocol> *_Nullable)customerJourneyController:(IDSCustomerJourneyController *_Nonnull)scanner intermediateUIForStep:(IDSEnterpriseRequiredAction)nextStep
DEPRECATED_MSG_ATTRIBUTE("method was deprecated since 10.6.0, please use customerJourneyController:intermediateVCForStep:enterpriseResponse instead");

@optional
/// Asks method whether intermediate view controller while uploading data to the backend should be presented during the journey
/// @param scanner of customer journey
/// @param currentStep which step's data is being submitted to the backend
- (IDSCustomerJourneyUIController <IDSCustomerJourneyUIUploadingProtocol> *_Nullable )customerJourneyController:(IDSCustomerJourneyController *_Nonnull)scanner intermediateUIForUploadingState:(IDSEnterpriseRequiredAction)currentStep;

@optional
/// Asks method whether intermediate view controller between different steps should be presented during the journey
/// @param scanner of customer journey
/// @param nextStep contains the information about the next required step
/// @param enterpriseResponse the API response containg information about the previous scan
- (IDSCustomerJourneyUIController <IDSCustomerJourneyUIUploadingProtocol> *_Nullable)customerJourneyController:(IDSCustomerJourneyController *_Nonnull)scanner intermediateVCForStep:(IDSEnterpriseRequiredActionState * _Nonnull)nextStep enterpriseResponse:(IDSEnterpriseResponse* _Nullable)enterpriseResponse;

@end

@interface IDSCustomerJourneyController : UIViewController

/**
 Delegate of IDSCustomerJourneyController
 */
@property (nullable ,nonatomic, weak) id<IDSCustomerJourneyControllerDelegate> delegate;

/**
 IEOS backend credentials that are used for submitting data
 */
@property (nonatomic, strong, nullable) IDSEnterpriseCredentials *credentials;

/**
 Set journeyDefinitionID if you know exact journey you want to perform
 */
@property (nonatomic, strong, nullable) NSString *journeyDefinitionID;

/**
 Use this closure to modify the request to the backend, which will be sent for the provided action
 */
@property (nonatomic, copy) void (^ _Nonnull prepareForEnterpriseRequest)(IDSEnterpriseRequestModifier *_Nonnull request, IDSEnterpriseRequiredActionState *_Nonnull requiredAction);

/// Default constructor
/// @param config configuration of customer journey
- (instancetype _Nonnull)initWithConfig:(IDSCustomerJourneyConfig * _Nullable)config;

@end
