//
//  IDSLivenessProtocols.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 28/04/2020.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

@class IDSLivenessOnlineViewController;
@class IDSLivenessOfflineViewController;
@class IDSEnterpriseResponse;
@class IDSLivenessResult;

/// Protocol events that could be triggered during the liveness process
@protocol IDSLivenessOnlineViewControllerProtocol <NSObject>

@required

/// Protocol method triggered once liveness has finished all actions and the response with next action from the backend is received
/// Be aware that liveness might not be the last action, hence you should analyse the response from the backend
/// and check the next requiredAction
/// @param livenessController controller itself
/// @param response from the backend after submitting liveness result. It should be analysed by the application in order to understand the next required action
- (void)didFinishLiveness:(IDSLivenessOnlineViewController * _Nonnull)livenessController
      withBackendResponse:(IDSEnterpriseResponse * _Nonnull)response;

@required

/// Triggered if any internal error occurs such as not granted camera permissions
/// @param livenessController controller itself
/// @param error with localised description
- (void)didFailLiveness:(IDSLivenessOnlineViewController * _Nonnull)livenessController
      withInternalError:(NSError * _Nonnull)error;

@required

/// Triggered by any networking errors that occur while submitting liveness request during the process
/// @param livenessController controller itself
/// @param error contains localised description from the backend and HTTP status code. Will return IDSNetworkingError.unkown if status code is not available for some reason
- (void)didFailLiveness:(IDSLivenessOnlineViewController * _Nonnull)livenessController
      withNetworkError:(NSError * _Nonnull)error;

@required
/// Trigger when user wants to cancel the liveness
/// @param livenessController controller itself
- (void)didCancelLiveness:(IDSLivenessOnlineViewController * _Nonnull)livenessController;

@optional

/// Protocol method triggered once liveness has finished all actions and data uplaoding to the backend has started
/// Be aware that liveness might not be the last action, hence you should analyse the response from the backend
/// and check the next requiredAction
/// @param livenessController controller itself
- (void)didStartUploadingLiveness:(IDSLivenessOnlineViewController * _Nonnull)livenessController;

@end

/// Protocol events that could be triggered during offline liveness scan
@protocol IDSLivenessOfflineViewControllerProtocol <NSObject>

@required

/// Delegate method, which will be called once liveness test is finished
/// @param livenessController the instance of liveness controller
/// @param result IDSLivenessResult, which contains high level outcome and all performed actions
- (void)didFinishLiveness:(IDSLivenessOfflineViewController * _Nonnull)livenessController
      withLivenessResult:(IDSLivenessResult * _Nonnull)result;

@required

/// Delegate method, which could return errors while performing liveness
/// @param livenessController the instance of liveness controller
/// @param error NSError describing the failure
- (void)didFailLiveness:(IDSLivenessOfflineViewController  * _Nonnull)livenessController
      withInternalError:(NSError * _Nonnull)error;

@required

///  Delegate method, which will pass face images while performing liveness detection.
///  It could be used to do the face match in the meantime while doing liveness. The number of calls will depend on the liveness engine configuration
///  @see IDSLivenessConfig numberOfFaceImages
/// @param image image of the person looking straight into the camera
- (void)receivedFace:(UIImage * _Nonnull)image;

@required
/// Trigger when user wants to cancel the liveness
/// @param livenessController controller itself
- (void)didCancelLiveness:(IDSLivenessOfflineViewController * _Nonnull)livenessController;

@end
