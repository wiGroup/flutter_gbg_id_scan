//
//  IDSEnterpriseImageQualityChecks.h
//  IDSWebServices
//
//  Created by Muhammad Nasir Ali on 15/02/2021.
//  Copyright © 2021 GB Group Plc. All rights reserved.
//

#import "IDSEnterpriseImageQualityCheck.h"

NS_ASSUME_NONNULL_BEGIN

@interface IDSEnterpriseImageQualityChecks : NSObject
@property (nonatomic, strong, nullable) IDSEnterpriseImageQualityCheck *blurCheck;
@property (nonatomic, strong, nullable) IDSEnterpriseImageQualityCheck *glareCheck;
@property (nonatomic, strong, nullable) IDSEnterpriseImageQualityCheck *lowResCheck;
@property (nonatomic, strong, nullable) IDSEnterpriseImageQualityCheck *boundryCheck;
@end

NS_ASSUME_NONNULL_END
