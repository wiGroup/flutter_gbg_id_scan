//
//  IDSLivenessOnlineViewController.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 27/04/2020.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJCS/IDSLivenessViewControllerBase.h>
#import <MJCS/IDSEnterpriseService.h>
#import <MJCS/IDSLivenessProtocols.h>

@class IDSLivenessViewControllerBase;
@class IDSLivenessOnlineViewController;

NS_ASSUME_NONNULL_BEGIN

/// Class, which performs liveness check and submits all data automatically to the backend
@interface IDSLivenessOnlineViewController : IDSLivenessViewControllerBase

/// Default constructor to initialise liveness controller, when default overlay will be used
/// @param journeyID should be retrieved from previous requests to the backend. It is used in order to send liveness
/// @param credentials backend credentials
/// @param lastStepResponse  once backend asks for liveness step it also sends specific liveness engine configuration, which needs to be extracted. In order to do so, you always should pass the response from the backend, when requiredAction was LIVENESS. If not passed, default configuration will be used
/// @param delegate delegate listener
- (instancetype)initWithJourneyID:(NSString * _Nonnull)journeyID
                      credentials:(IDSEnterpriseCredentials * _Nonnull)credentials
                 lastStepResponse:(IDSEnterpriseResponse * _Nonnull)lastStepResponse
                         delegate:(id <IDSLivenessOnlineViewControllerProtocol> _Nullable)delegate;

/// Main constructor, which accepts custom overlay
/// @param controller overlay, which will be displayed on top of the camera feed. It should adhere to protocols specified
/// @param journeyID should be retrieved from previous requests to the backend. It is used in order to send liveness information to the specific journey
/// @param credentials backend credentials
/// @param lastStepResponse once backend asks for liveness step it also sends specific liveness engine configuration, which needs to be extracted. In order to do so, you always should pass the response from the backend, when requiredAction was LIVENESS. If not passed, default configuration will be used
/// @param delegate listener
- (instancetype)initWithOverlayController:(UIViewController<IDSLivenessViewProtocol> *)controller
                       journeyID:(NSString *)journeyID
     credentials:(id)credentials
lastStepResponse:(id)lastStepResponse
                                 delegate:(id)delegate;


@end

NS_ASSUME_NONNULL_END
