//
//  IDSDocumentScannerController.h
//  MJCS SDK
//
//  Created by Stanislav Kozyrev <s.kozyrev@idscan.co.uk>.
//  Modified by Tomas Zemaitis <t.zemaitis@idscan.co.uk>.
//  Modified by Edvardas Maslauskas <e.maslauskas@idscan.com>
//  Copyright (c) 2014 IDScan Biometrics Ltd. All rights reserved.
//

#import <MJCS/IDSBase.h>
#import <UIKit/UIKit.h>

@class IDSDocumentScannerController;
@class IDSDocumentScannerConfig;

#pragma mark - Scanner delegate protocol

/**
 * The metadata object containing document scanning result. The
 * value is of type IDSMetadataDocumentObject if scanner type is
 * IDSDocumentScannerTypeDocument
 */
MJCS_EXPORT NSString * _Nonnull const IDSDocumentScannerInfoMetadataObject;

/**
 * @brief The extracted image on which scanner procedure was performed. The
 * Value is of type UIImage.
 */
MJCS_EXPORT NSString * _Nonnull const IDSDocumentScannerInfoImage;


/**
 * The IDSDocumentCaptureControllerDelegate protocol defines methods
 * that your delegate object must implement to interact with the document
 * capture interface.
 */
MJCS_CLASS_EXPORT
@protocol IDSDocumentScannerControllerDelegate <NSObject>

@optional
/**
 * Tells the delegate that the scanner is unable to perform its duties.
 */
- (void)documentScannerController:(IDSDocumentScannerController *_Nonnull)scanner didFailWithError:(NSError *_Nullable)error;

@optional
/**
 * Tells the delegate that the user cancelled the scan operation.
 */
- (void)documentScannerControllerDidCancel:(IDSDocumentScannerController *_Nonnull)scanner;

@optional
/**
 * Tells the delegate that the scanner recognized a document object
 * in the image.
 */
- (void)documentScannerController:(IDSDocumentScannerController *_Nonnull)scanner didFinishScanningWithInfo:(NSDictionary *_Nonnull)info;

@end

/**
 * The IDSDocumentScannerController class manages framework-supplied user
 * interfaces for scanning documents. A document scanner controller manages user
 * interactions and delivers the results of those interactions to a delegate
 * object.
 */
MJCS_CLASS_EXPORT
@interface IDSDocumentScannerController : UIViewController

/**
 * The document scanner’s delegate object.
 */
@property (weak, nonatomic, nullable) id<IDSDocumentScannerControllerDelegate> delegate;

/// Main constructor to initialise document scanner
/// @param config of the document scanner
- (instancetype _Nonnull )initWithScannerConfig:(IDSDocumentScannerConfig * _Nonnull)config;

/// Allows to hide cancel button on the screen
/// @param hidden BOOL whether cancel button should be hidden
- (void)setCancelButtonHidden:(BOOL)hidden;

@end
