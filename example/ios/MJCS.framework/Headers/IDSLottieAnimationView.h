//
//  IDSLottieAnimationView.h
//  MJCS
//
//  Created by Muhammad Nasir Ali on 23/10/2020.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^IDSLottieAnimationCompletionBlock)(BOOL);

@interface IDSLottieAnimationView: UIView
@property(nonatomic, strong) NSString *animationFileName;
@property(nonatomic, assign) CGFloat animationSpeed;
@property(nonatomic, assign) NSInteger animationRepeatCount;
@property(nonatomic, assign) UIViewContentMode animationContentMode;
- (instancetype)initWithAnimationFileName:(NSString*)fileName;
- (void)playAnimationWithCompletion:(nullable IDSLottieAnimationCompletionBlock)completion;
- (void)playAnimationInRange:(NSRange)range completion:(IDSLottieAnimationCompletionBlock)completion;
- (void)resetAnimation;
@end

NS_ASSUME_NONNULL_END
