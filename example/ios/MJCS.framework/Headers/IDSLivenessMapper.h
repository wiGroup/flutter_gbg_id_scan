//
//  IDSLivenessResultMapper.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 13/06/2018.
//  Copyright © 2018 IDScan Biometrics. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IDSLivenessResult;
@class IDSEnterpriseRequestLiveness;
@class IDSEnterpriseResponse;
@class IDSLivenessConfig;

@interface IDSLivenessMapper : NSObject

/// Maps liveness request from the liveness result
/// @param livenessResult result of the liveness scan
+ (nonnull IDSEnterpriseRequestLiveness *)mapEnterpriseRequest:(nonnull id)livenessResult;

/// To initialise liveness with proper config, we need to extract it from the previous response from the backend
/// when required action was LIVENESS
/// @param response Previous response from the backend when required action was LIVENESS
+ (nonnull IDSLivenessConfig *)mapLivenessConfig:(nonnull IDSEnterpriseResponse *)response;
@end
