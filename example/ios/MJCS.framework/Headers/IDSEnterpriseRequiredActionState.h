//
//  IDSRequiredActionState.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas on 04/02/2020.
//  Copyright © 2020 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
Enumerated value to show the required action that needs to be proceeded by the client.
*/
typedef NS_ENUM(NSInteger, IDSEnterpriseRequiredAction) {
    IDSRequiredActionFront = 0,
    IDSRequiredActionBack,
    IDSRequiredActionSelfie,
    IDSRequiredActionLiveness,
    IDSRequiredActionPassiveLiveness,
    IDSRequiredActionAddressDocument,
    IDSRequiredActionNFC,
    IDSRequiredActionNone,
    IDSRequiredActionUnknown
};

/**
Enumerated value to show which attempt of the step is being performed in triple scan cycle if enabled on the backend
*/
typedef NS_ENUM(NSInteger, IDSEnterpriseRequiredActionAttempt) {
    IDSRequiredActionAttemptFirst = 0,
    IDSRequiredActionAttemptSecond,
    IDSRequiredActionAttemptThird,
    IDSRequiredActionAttemptUnknown
};

@interface IDSEnterpriseRequiredActionState : NSObject

/// Property showing, which action needs to be taken by the client.
@property (nonatomic, assign, readonly) IDSEnterpriseRequiredAction action;

/// Property showing, which attempt is currently being done on the action
@property (nonatomic, assign, readonly) IDSEnterpriseRequiredActionAttempt attempt;

/// Property showing whether required action is a first attempt or a retry
@property (nonatomic, assign, readonly) BOOL isRetry;

/**
 Property showing, which action will need to be taken after current action.
 It will only contain data when current action is NFC
 */
@property (nonatomic, assign, readonly) IDSEnterpriseRequiredAction followingAction;

/// Property returning raw string of requiredAction as returned by the backend
@property (nonatomic, nonnull, readonly) NSString *rawAction;


@end

NS_ASSUME_NONNULL_END
