import 'package:flutter/material.dart';

import 'package:flutter_gbg_id_scan/flutter_gbg_id_scan.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  _onStartUserJourneyPressed() async {
    final config = GbgConfig(
      baseUrl: 'https://poc.idscan.cloud',
      journeyId: 'f0222146-d9d0-4ae6-b665-d07aaef47d49',
      password: '!Mon01+',
      username: 'VMSuper',
    );

    FlutterGbgIdScan(config, config.journeyId).startUserJourney.then((value) {
      print(value);
    }, onError: (error) {
      print(error.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 246, 246, 246),
        appBar: AppBar(
          title: const Text('GBG|ID Scan demo app'),
          backgroundColor: Color.fromARGB(255, 55, 63, 67),
        ),
        body: Center(
          child: ListView(
              padding: EdgeInsets.all(20.0),
              shrinkWrap: true,
              children: <Widget>[
                SizedBox(height: 20),
                RaisedButton(
                  color: Color.fromARGB(255, 134, 250, 194),
                  textColor: Color.fromARGB(255, 33, 28, 92),
                  child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Text(
                        "Start User Journey",
                        style: TextStyle(fontSize: 16),
                      )),
                  onPressed: _onStartUserJourneyPressed,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(
                          color: Color.fromARGB(255, 134, 250, 194))),
                ),
              ]),
        ),
        bottomSheet: Container(
          padding: EdgeInsets.all(10.0),
          color: Colors.white,
          width: double.infinity,
          child: Text("Developed with ♥ by Leonardo Lerasse and Shaun. (O_O)"),
        ),
      ),
    );
  }
}
