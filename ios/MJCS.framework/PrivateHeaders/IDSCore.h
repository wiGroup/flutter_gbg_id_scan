//
//  IDSCore.h
//  MJCS SDK
//
//  Created by Stanislav Kozyrev <s.kozyrev@idscan.co.uk>.
//  Copyright (c) 2014 IDScan Biometrics Ltd. All rights reserved.
//

#import <MJCS/IDSBase.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IDSCore : NSObject

+ (NSBundle *)bundle;

+ (nullable NSString *)pathForResource:(NSString *)name ofType:(NSString *)ext;

+ (nullable NSString *)pathForResource:(NSString *)name ofType:(NSString *)ext inDirectory:(NSString *)subpath;

+ (nullable NSString *)pathForResource:(NSString *)name ofType:(NSString *)ext inDirectory:(NSString *)subpath forLocalization:(NSString *)localizationName;

+ (void)mjcs_log:(NSString *)logString;

+ (BOOL)isWhitelabelEnabled;

+ (nullable UIImage *)loadImage:(NSString *)imageFileName;

/// Extension for liveness since we can't import it to swift headers
/// @param action action value
+ (NSString* _Nonnull )IDSLivenessActionString:(NSInteger)action;

@end
NS_ASSUME_NONNULL_END
