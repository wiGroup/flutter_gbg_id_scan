//
//  IDSEnterpriseSession.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas on 13/03/2017.
//  Copyright © 2017 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IDSEnterpriseCredentials;

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, IDSEnterpriseInternalAuthenticationResult) {
    IDSEnterpriseInternalAuthenticationResultSuccessful = 0,
    IDSEnterpriseInternalAuthenticationResultInvalidCredentials,
    IDSEnterpriseInternalAuthenticationResultError
};

@interface IDSEnterpriseSession : NSObject

+ (void)authenticateEnterpriseSessionWithCredentials:(IDSEnterpriseCredentials *)credentials async:(BOOL)isAsync
                                      WithCompletion:(void (^)(IDSEnterpriseInternalAuthenticationResult result, NSError *_Nullable error, NSString *_Nullable token))completionHandler;

+ (void)postRequestWithPath:(NSString *)path
                     params:(NSDictionary *_Nullable)params
                credentials:(IDSEnterpriseCredentials *)credentials
                   progress:(void (^_Nullable)(NSProgress * uploadProgress))progressHandler
          completionHandler:(void (^)(id _Nullable responseObject, NSError *_Nullable error))completionHandler;

+ (void)getRequestWithPath:(NSString *)path
                    params:(NSDictionary *_Nullable)params
               credentials:(IDSEnterpriseCredentials *)credentials
                  progress:(void (^_Nullable)(NSProgress * uploadProgress))progressHandler
         completionHandler:(void (^)(id _Nullable responseObject, NSError *_Nullable error))completionHandler;

+ (void)uploadTaskWithRequest:(NSMutableURLRequest *)request
                  credentials:(IDSEnterpriseCredentials *)credentials
                     progress:(void (^_Nullable)(NSProgress * uploadProgress))progressHandler
            completionHandler:(void (^)(id _Nullable parameters, NSError *_Nullable error))completionHandler;
@end
NS_ASSUME_NONNULL_END
