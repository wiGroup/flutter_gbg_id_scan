//
//  IDSLivenessMapper+Private.h
//  MJCS
//
//  Created by Edvardas Maslauskas on 29/06/2020.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//


NS_ASSUME_NONNULL_BEGIN

@interface IDSLivenessMapper (Private)

/// We can't import liveness dependency into the swift code, hence to fix issue MJCS-2101, when backend requires
/// to get at least 1 selfie before the result, we have this helper function
/// @param result liveness result
+ (nullable UIImage *)firstLivenessActionImage:(nonnull id)result;
@end

NS_ASSUME_NONNULL_END
