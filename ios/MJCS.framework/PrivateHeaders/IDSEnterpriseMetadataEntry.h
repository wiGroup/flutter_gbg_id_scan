//
//  IDSEnterpriseMetadataEntry.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IDSEnterpriseRequiredActionState;
typedef NS_ENUM(NSInteger, IDSEnterpriseRequiredAction);

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, IDSEnterpriseMetadataEntryType) {
    IDSEnterpriseMetadataEntryTypeSimple = 0,
    IDSEnterpriseMetadataEntryTypeComplex
};

@interface IDSEnterpriseMetadataEntry : NSObject


/// Key of the metadata entry
@property (nonatomic, nullable, readonly) NSString *name;

/// Value of the metadata entry
@property (nonatomic, nullable, readonly) NSString *value;

/// Type of the entry
@property (assign, readonly) IDSEnterpriseMetadataEntryType type;

/// Step for which the entry needs to be displayed
@property (assign, readonly) IDSEnterpriseRequiredAction step;


@end

NS_ASSUME_NONNULL_END
