//
//  MJCS.h
//  MJCS SDK
//
//  Created by Edvardas Maslauskas <e.maslauskas@idscan.com>.
//  Copyright (c) 2017 IDScan Biometrics Ltd. All rights reserved.
//

#import <MJCS/IDSDocumentScannerController.h>
#import <MJCS/IDSCustomerJourneyController.h>
#import <MJCS/IDSEnterpriseService.h>
#import <MJCS/IDSWebServices.h>
#import <MJCS/IDESDocument.h>
#import <MJCS/IDSLivenessMapper.h>
#import <MJCS/IDESProfile.h>
#import <MJCS/IDESProfileManager.h>
#import <MJCS/IDESCountry.h>
#import <MJCS/IDSCustomerJourneyUIController.h>
#import <MJCS/IDSLivenessOnlineViewController.h>
#import <MJCS/IDSLivenessOfflineViewController.h>
#import <MJCS/IDSLivenessViewControllerBase.h>
#import <MJCS/IDSCameraPublic.h>
#import <MJCS/IDSLottieAnimationView.h>
#import <MJCS/IDSCameraController_Private.h>

@class IDSGlobalConfig;
@protocol IDSCameraCaptureControllerDelegate;

@interface MJCS : NSObject

/**
 Loads the SDK modules and required resources synchronously. You need to call this method before using it
 */
+ (void)loadSDK;

/**
 Loads the SDK modules and required resources asynchronously. You need to call this method before using it

 @param completion a block which will be called once init is finished. It might return an exception if something goes wrong
 */
+ (void)loadWithCompletion :(void (^_Nonnull)(BOOL, NSException*_Nullable))completion;

/**
 Method used to swap document processing profile to the one, which is specified

 @param profile an object, which needs to be loaded for document processing engine
 @param completion a block, which will be called once loading is finished
 */
+ (void)swapWithProfile :(IDESProfile *_Nonnull)profile withCompletion:(void (^_Nonnull)(BOOL, NSException*_Nullable))completion;

/**
 Method used to swap document processing profile with the specified tag

 @param tag name of the profile
 @param completion a block, which will be called once loading is finished
 */
+ (void)swapWithTag :(NSString *_Nonnull)tag withCompletion:(void (^_Nonnull)(BOOL, NSException*_Nullable))completion;

/**
 Method, which unloads all resources of the SDK from the memory
 Should be used when SDK is no longer required by the application
 */
+ (void)disposeSDK;

/**
Shared instance of MJCS configuration instance
 @warning SDK needs to be loaded first in order for this instance to be returned
*/
+ (MJCS * _Nullable)sharedInstance;

/**
Global config of the MJCS
*/
@property (nonatomic, nonnull, setter=setGlobalConfig:) IDSGlobalConfig *globalConfig;

/**
Returns a boolean whether required resources are loaded
*/
@property (nonatomic, assign) BOOL resourcesLoaded;

@property (nonatomic, nonnull, readonly) IDESProfile *currentProfile;

@end
