//
//  IDSEnterpriseCredentials.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas on 03/03/2017.
//  Copyright © 2017 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Credentials object, which is used for enterprise API authentication
 */
@interface IDSEnterpriseCredentials : NSObject

/**
 User login name
 */
@property (nonatomic, nullable, readonly) NSString *username;

/**
 User password
 */
@property (nonatomic, nullable, readonly) NSString *password;

/**
 User token
 */
@property (nonatomic, nullable, readonly) NSString *token;

/**
 Backend API url prefix, example: https://yourServer
 */
@property (nonatomic, nonnull, readonly) NSString *urlPrefix;

@property (nonatomic, nullable) NSSet <NSData *> *pinningCertificates;

/**
 IDSEnterpriseCredentials constructor
 
 @param token for login
 @param urlPrefix Backend API url prefix, example: https://yourServer
 @return IDSEnterpriseCredentials object
 */
- (instancetype _Nonnull)initWithToken:(NSString * _Nonnull)token
                             urlPrefix:(NSString * _Nonnull)urlPrefix;

/**
 IDSEnterpriseCredentials constructor
 
 @param username User login name
 @param password User password
 @param urlPrefix Backend API url prefix, example: https://yourServer
 @return IDSEnterpriseCredentials object
 */
- (instancetype _Nonnull)initWithUsername:(NSString * _Nonnull)username
                                 password:(NSString * _Nonnull)password
                                urlPrefix:(NSString * _Nonnull)urlPrefix;
/**
 Method which returns json string of credentials using Enterprise API authenticate structure

 @return JSON string
 */
- (NSString * _Nonnull)getCredentialsRequestJSON;

@end
