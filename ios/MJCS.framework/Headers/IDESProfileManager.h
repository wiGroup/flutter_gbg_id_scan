//
//  IDESProfileManager+IDESProfileManager.h
//  idesv2
//
//  Created by Abdulrhman Babelli on 9/6/18.
//  Copyright © 2018 GBG Plc All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IDESProfile.h"

/**
 The constant of the default profile. Default profile is the one, which is added to the initial application bundle
 */
extern NSString *_Nonnull const IDESDefaultProfile;

/**
 The interface, which is responsible of managing the profiles of the engine. Profile consists of multiple resources that are responsible for scanning specific countries/documents
 */
@interface IDESProfileManager : NSObject

/**
 A method, which will install the profile with a provided tag name from the specified zip file path. The method has a logic to only re-install the profile if it has different version and it will not install the ones that already exist.
 
 @param pathToZipFile full path to the .zip file of the profile
 @param tag your tag name, which will be used to name the profile and access it later on
 @param error returns an error object if something went wrong. In swift it will throw an error
 @return BOOL whether the install was successful
 */
+ (BOOL)install:(NSString *_Nonnull)pathToZipFile withTag:(NSString *_Nonnull)tag error:(NSError *_Nullable*_Nullable)error;

/**
 Force to install the profile without checking the existing version of the tag. It could be used to re-install corrupted profiles or downgrade versions of them

 @param pathToZipFile full path to the .zip file of the profile
 @param tag name, which will be used to name the profile and access it later on
 @return BOOL whether the install was successful
 */
+ (BOOL)forceInstall:(NSString *_Nonnull)pathToZipFile withTag:(NSString *_Nonnull)tag;

/**
 Delete the installed profile from the filesystem using a tag name

 @param tag name, which was used to install the profile
 @return BOOL whether the removal was successful
 */
+ (BOOL)removeWithTag:(NSString *_Nonnull)tag;

/**
 Delete the installed profile from the filesystem using the profile object

 @param profile object, which needs to be removed
 @return BOOL whether the removal was successful
 */
+ (BOOL)removeWithProfile:(IDESProfile *_Nonnull)profile;

/**
 Get and access the profile object of the specified tag

 @param tag name, of the required profile
 @return IDESProfile of the specified tag name
 */
+ (IDESProfile *_Nullable)getProfile:(NSString *_Nonnull)tag;

/**
 Lists all the profiles that were installed into the system

 @return Array of IDESProfile that are installed
 */
+ (NSArray<IDESProfile *> *_Nonnull)listProfiles;

/**
 Lists all the profile tags that were installed into the system

 @return Array of tags that are installed
 */
+ (NSArray<NSString *> *_Nonnull)listTags;

@end
