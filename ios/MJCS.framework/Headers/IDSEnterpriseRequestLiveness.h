//
//  IDSEnterpriseRequestLiveness.h
//  IDSWebServices
//
//  Created by Dicle Yilmaz on 5.06.2018.
//  Copyright © 2018 IDscan Biometrics Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDSEnterpriseActionDetail.h"
#import "IDSEnterpriseCredentials.h"

@interface IDSEnterpriseRequestLiveness : NSObject

/**
 The identifier for this person internally to the onboarding suite.
 */
@property (nonatomic, strong, nullable) NSString *journeyId;

/**
 The final result of the liveness action
 */
@property (nonatomic, strong, nullable) NSNumber *finalResult;

/**
Failure reason of the liveness process if there's any
*/
@property (nonatomic, strong, nullable) NSNumber *failureReason;

/**
 A mutable set of processed action details of liveness.
 */
@property (nonatomic, strong, nonnull) NSArray<IDSEnterpriseActionDetail *> *actionDetails;

/**
 Method which generates request's payload dictionary for liveness
 
 @param completionHandler returns parameters and error object if mandatory fields were not set
 */
- (void)generateRequestParameters:(void (^_Nonnull)(NSDictionary *_Nullable parameters, NSError *_Nullable error))completionHandler;

/**
 Credentials property used for authenticating the request
 */
@property (nonatomic, strong, nullable) IDSEnterpriseCredentials *credentials;

/**
 @internal
 */
@property (nonatomic, strong, nonnull) NSString *c;

@end
