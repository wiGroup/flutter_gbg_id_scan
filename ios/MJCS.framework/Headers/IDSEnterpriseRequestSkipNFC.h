//
//  IDSEnterpriseRequestSkipNFC.h
//  IDSWebServices
//
//  Created by Edvardas Maslauskas.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDSEnterpriseCredentials.h"

@interface IDSEnterpriseRequestSkipNFC : NSObject

/**
 The identifier for this person internally to the onboarding suite.
 */
@property (nonatomic, strong, nullable) NSString *journeyId;

/**
 The final result of the liveness action
 */
@property (nonatomic, strong, nullable) NSString *failureReason;

/**
 Method which generates request's payload dictionary for liveness
 
 @param completionHandler returns parameters and error object if mandatory fields were not set
 */
- (void)generateRequestParameters:(void (^_Nonnull)(NSDictionary *_Nullable parameters, NSError *_Nullable error))completionHandler;

/**
 Credentials property used for authenticating the request
 */
@property (nonatomic, strong, nullable) IDSEnterpriseCredentials *credentials;

@end
