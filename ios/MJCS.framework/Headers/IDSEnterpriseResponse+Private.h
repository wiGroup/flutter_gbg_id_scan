//
//  IDSEnterpriseResponse_Private.h
//  IDSWebServices
//
//  Created by David Okun on 15/09/2016.
//  Copyright © 2016 IDscan Biometrics Ltd. All rights reserved.
//

#import "IDSEnterpriseResponse.h"

@interface IDSEnterpriseResponse (Private)

- (instancetype)initWithPayload:(NSDictionary *)payload;
- (instancetype)initWithPassiveLivenessPayload:(NSDictionary *)payload;
- (void)updateEnterpriseRequiredAction:(IDSEnterpriseRequiredAction)action;
@end
