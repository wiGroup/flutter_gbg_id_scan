//
//  IDSEnterpriseJourneyResponse.h
//  IDSWebServices
//
//  Created by Dicle Yilmaz on 14/09/2017.
//  Copyright © 2017 IDscan Biometrics Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+IDSEnterpriseService.h"
#import "IDSEnterpriseProcessedDocument.h"

/**
 High Level Result needed state constant
 */
extern NSString * _Nonnull const kIDSEnterpriseHighLevelResultNeeded;
/**
 High Level Result not needed state constant
 */
extern NSString * _Nonnull const kIDSEnterpriseHighLevelResultNotNeeded;
/**
 High Level Result passed state constant
 */
extern NSString * _Nonnull const kIDSEnterpriseHighLevelResultPassed;
/**
 High Level Result failed state constant
 */
extern NSString * _Nonnull const kIDSEnterpriseHighLevelResultFailed;
/**
 High Level Result skipped state constant
 */
extern NSString * _Nonnull const kIDSEnterpriseHighLevelResultSkipped;
/**
 High Level Result undetermined state constant
 */
extern NSString * _Nonnull const kIDSEnterpriseHighLevelResultUndetermined;
/**
 High Level Result unknown state constant
 */
extern NSString * _Nonnull const kIDSEnterpriseHighLevelResultUnknown;
/**
 High Level Result success needed state constant
 */
extern NSString * _Nonnull const kIDSEnterpriseHighLevelResultSuccessNeeded;

@interface IDSEnterpriseJourneyResponse : NSObject

/**
 The date and time when the entry was created and the journey was started.
 */
@property (nonatomic, nullable, readonly) NSDate *initiatedDateTime;

/**
 The identifier for this person internally to the onboarding suite.
 */
@property (nonatomic, nonnull, readonly) NSString *journeyID;

/**
 The high level result of the journey as a whole. Keep in mind that multiple documents within the journey could have different results, but the business logic of the journey contributes to how this journey is graded.
 */
@property (nonatomic, assign, readonly) IDSEnterpriseResponseHighLevelResult highLevelResult;

/**
 The details of the high level result decision. This will include the reasons that caused the journey to refer.
 */
@property (nonatomic, nonnull, readonly) NSDictionary<NSString *, NSString *> *highLevelResultDetails;

/**
 Shows additional data relevant to the particular scan.
 */
@property (nonatomic, nullable, readonly) NSDictionary *additionalData;

/**
 An array of processed documents within the journey. See documentation for specific object.
 */
@property (nonatomic, nullable, readonly) NSArray<IDSEnterpriseProcessedDocument *> *processedDocuments;

/**
 The reference number, which was used to determine which journey would be detailed.
 */
@property (nonatomic, nonnull, readonly) NSString *referenceNumber;

@end
