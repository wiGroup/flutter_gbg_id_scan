//
//  IDSEnterpriseImageQualityCheck.h
//  IDSWebServices
//
//  Created by Muhammad Nasir Ali on 15/02/2021.
//  Copyright © 2021 GB Group Plc. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Returns status of the image quality check. For detailed information about
 * @return status of the image quality check.
 */
typedef NS_ENUM(NSInteger, IDSEnterpriseImageQualityCheckStatus) {
    /**
     *  If status is undetermined.
     */
    IDSEnterpriseImageQualityCheckStatusUndetermined = 0,
    /**
     *  If image quality is passing certain criterias.
     */
    IDSEnterpriseImageQualityCheckStatusGood = 1,
    /**
     *  If image quality is failing certain criterias
     */
    IDSEnterpriseImageQualityCheckStatusBad = 2,

};

/**
 * Returns type of the image quality check.
 * @return type of the image quality check.
 */
typedef NS_ENUM(NSInteger, IDSEnterpriseImageQualityCheckType) {
    /**
     *  If type is undetermined.
     */
    IDSEnterpriseImageQualityCheckTypeUnknown = 0,
    /**
     *  If type is blur.
     */
    IDSEnterpriseImageQualityCheckTypeBlur = 1,
    /**
     *  If type is glare.
     */
    IDSEnterpriseImageQualityCheckTypeGlare = 2,
    /**
     *  If type is low-Res.
     */
    IDSEnterpriseImageQualityCheckTypeLowRes = 3,
    /**
     *  If type is boundary.
     */
    IDSEnterpriseImageQualityCheckTypeBoundary = 4,

};

/**
 *  Object which explains a particular image quality check performed on a returned document
 */
@interface IDSEnterpriseImageQualityCheck : NSObject

/**
 * Returns status of the image quality check.
 * @return status of image quality check.
 */
@property (nonatomic, readonly, assign) IDSEnterpriseImageQualityCheckStatus status;

/**
 * Returns type of the image quality check.
 * @return type of image quality check.
 */
@property (nonatomic, readonly, assign) IDSEnterpriseImageQualityCheckType type;

@end
