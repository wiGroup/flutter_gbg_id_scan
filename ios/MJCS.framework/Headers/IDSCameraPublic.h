//
//  IDSCameraPublic.h
//  MJCS
//
//  Created by Muhammad Nasir Ali on 08/12/2020.
//  Copyright © 2020 GB Group Plc. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

static NSString* const IDSDocumentCameraCaptureActionKey = @"IDSCaptureAction";
static NSString* const IDSDocumentCameraCaptureActionFrontValue = @"IDSCaptureFront";
static NSString* const IDSDocumentCameraCaptureActionBackValue = @"IDSCaptureBack";
static NSString* const IDSHelpIconExtendedDelayCompletedKey = @"extendedDelayCompleted";

NS_ASSUME_NONNULL_END
