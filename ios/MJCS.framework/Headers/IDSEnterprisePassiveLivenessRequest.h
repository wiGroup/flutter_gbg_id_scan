//
//  IDSEnterprisePassiveLivenessRequest.h
//  IDSWebServices
//
//  Created by Muhammad Nasir Ali on 02/10/2020.
//  Copyright © 2020 IDscan Biometrics Ltd. All rights reserved.
//

#import "IDSEnterpriseCredentials.h"
@class UIImage;

NS_ASSUME_NONNULL_BEGIN
@interface IDSEnterprisePassiveLivenessRequest : NSObject
/**
 Credentials property used for authenticating the request
 */
@property (nonatomic, strong) IDSEnterpriseCredentials *credentials;

/**
 The identifier for this person internally to the onboarding suite.
 */
@property (nonatomic, strong) NSString *journeyId;

/**
 The identifier for this person internally to the onboarding suite.
 */
@property (nonatomic, strong, nullable) UIImage *inputImage;

/**
 Method which generates request's payload dictionary for liveness
 @param completionHandler returns parameters and error object if mandatory fields were not set
 */
- (void)generateRequestParameters:(void (^)(NSDictionary *_Nullable parameters, NSError *_Nullable error))completionHandler;
@end
NS_ASSUME_NONNULL_END
