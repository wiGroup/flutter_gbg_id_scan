#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flutter_gbg_id_scan.podspec' to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutter_gbg_id_scan'
  s.version          = '1.1.0'
  s.summary          = 'Flutter plugin implementation of the GBG ID SCAN Native ios sdks'
  s.description      = <<-DESC
GBG documentation - https://docs.idscan.com/docs/mjcs-ios-sdk-8-5/introduction/
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Shaun Norton' => 'shaun.norton47@gmail.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.dependency 'Flutter'
  s.dependency 'lottie-ios'
  s.preserve_paths = 'MJCS.framework', 'ZipZap.framework', 'FLAnimatedImage.framework', 'IDSLiveness.framework', 'AFNetworking.framework'
  s.xcconfig = { 'OTHER_LDFLAGS' => '-framework MJCS -framework ZipZap -framework FLAnimatedImage -framework IDSLiveness -framework AFNetworking' }
  s.vendored_frameworks = 'MJCS.framework', 'ZipZap.framework', 'FLAnimatedImage.framework', 'IDSLiveness.framework', 'AFNetworking.framework'
  s.platform = :ios, '11.0'

  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
  s.swift_version = '5.0'
end
