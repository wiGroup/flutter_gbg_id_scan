#import "FlutterGbgIdScanPlugin.h"
#if __has_include(<flutter_gbg_id_scan/flutter_gbg_id_scan-Swift.h>)
#import <flutter_gbg_id_scan/flutter_gbg_id_scan-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_gbg_id_scan-Swift.h"
#endif

@implementation FlutterGbgIdScanPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterGbgIdScanPlugin registerWithRegistrar:registrar];
}
@end
