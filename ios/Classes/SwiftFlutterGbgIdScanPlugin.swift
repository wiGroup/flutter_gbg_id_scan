import Flutter
import UIKit
import MJCS
#if canImport(IDSLiveness)
import IDSLiveness
#endif

public class SwiftFlutterGbgIdScanPlugin: NSObject, FlutterPlugin {
    var plugin: SwiftKycPlugin!
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "com.spot.plugin/gbgidscan", binaryMessenger: registrar.messenger())
        let instance = SwiftFlutterGbgIdScanPlugin()
        
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if (call.method == "startUserJourney") {
            DispatchQueue.main.async {
                MJCS.loadSDK()
                
                MJCS.sharedInstance()!.globalConfig = IDSGlobalConfig.builder().withCustomLocalization(tableName: "localizable-spot", inBundle: nil)
                self.plugin = SwiftKycPlugin()
                self.plugin.result = result
                self.plugin.unload = {
                    // TODO fix dispose SDK
                    // MJCS.disposeSDK()
                }
                
                if let customerController = self.plugin.configureKYC(call: call) {
                    UIApplication.shared.keyWindow?.rootViewController?.present(customerController, animated: true, completion: nil)
                }
            }
        } else {
            result(FlutterMethodNotImplemented)
        }
    }
}

class SwiftKycPlugin: NSObject {
    final var USER_CANCELLED_CODE = "499"
    final var SESSION_TIMEOUT_CODE = "2002"
    var result: FlutterResult!
    var unload: (() -> Void)!
    
    func configureKYC(call: FlutterMethodCall) -> IDSCustomerJourneyController? {
        if let args = call.arguments as? Dictionary<String, Any>,
            let baseUrl = args["baseUrl"] as? String,
            let username = args["username"] as? String,
            let password = args["password"] as? String,
            let journeyDefId = args["journeyId"] as? String
        {
            let imgQualityConfig = IDSDocumentScannerImgQualityConfig.builder()
                .withBlurCheckEnabled(true)
                .withGlareCheckEnabled(true)
                .withBoundaryCheckEnabled(true)
                .withLowResolutionCheckEnabled(true)
            let config = IDSCustomerJourneyConfig.builder()
                .withDocumentScannerConfig(IDSDocumentScannerConfig.builder()
                    .withUsingCroppedDocumentImages(true)
                    .withImgQualityConfig(imgQualityConfig)
                    .withCaptureButtonAppearTime(12))
            
            let credentials = IDSEnterpriseCredentials.init(username: username, password: password, urlPrefix: baseUrl)
            
            let customerController = IDSCustomerJourneyController(config: config)
            customerController.journeyDefinitionID = journeyDefId
            customerController.credentials = credentials
            customerController.delegate = self
            customerController.modalPresentationStyle = .fullScreen
            
            return customerController
        } else {
            self.unload()
            
            result(FlutterError(code: self.SESSION_TIMEOUT_CODE, message: "Bad arguments", details: nil))
            return nil
        }
    }
}

extension SwiftKycPlugin: IDSCustomerJourneyControllerDelegate {
    func customerJourneyController(_ scanner: IDSCustomerJourneyController, didFinishJourneyWithResult result: IDSEnterpriseJourneyResponse?) {
        self.unload()
        
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {
            self.result(result?.journeyID)
        })
    }
    
    func customerJourneyController(_ scanner: IDSCustomerJourneyController, didFailWithError error: Error?) {
        self.unload()
        
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {
            self.result(FlutterError(code: error?.localizedDescription ?? "Unknown Error", message: "User Journey Failed", details: "failed"))
        })
    }
    
    func customerJourneyControllerDidCancel(_ scanner: IDSCustomerJourneyController) {
        self.unload()
        
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {
            self.result(FlutterError(code: self.USER_CANCELLED_CODE, message: "User Journey Cancelled", details: "User Journey Failed"))
        })
    }
}
