//
//  IDSLivenessConfig.h
//  IDSLiveness
//
//  Created by Edvardas Maslauskas on 24/04/2018.
//  Copyright © 2018 GB Group plc ('GBG'). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IDSLivenessConfig : NSObject

/**
 A maximum time in seconds allowed for action to be performed before it times out
 */
@property (nonatomic, strong, nonnull) NSNumber *actionTimeout;

/**
 A number of actions that is required to perform in order to pass a liveness
 */
@property (nonatomic, strong, nonnull) NSNumber *numberOfActions;

/**
 A number of how many frames of a person looking straight liveness should capture
 
 @discussion The main property of this property is depending on the number set the identical amount of person's face
 images will be send to the callback, which is mainly used to perform a facematch
 @see onFaceImage callback in IDSLivenessDetection
 */
@property (nonatomic, strong, nonnull) NSNumber *numberOfFaceImages;

/**
 A number of maximum failures of actions that are allowed in order to pass a liveness test
 */
@property (nonatomic, strong, nonnull) NSNumber *numberOfMaxFailures;

/**
 A number in seconds of a minimum time for each action
 
 @discussion on powerful devices most of the time actions are passing too quickly,
 hence people need to change actions very fast, which makes user experience worse.
 */
@property (nonatomic, strong, nonnull) NSNumber *minTimeoutTime;

/**
 * A number of frames that must be seen containing an action for it to pass.
 * For tilt actions we will now expect to see a change in angle in the correct direction for this number of
 * frames.
 */
@property (nonatomic, strong, nonnull) NSNumber *passFrames;

/**
 * The maximum number of jumps in the video feed before we fail liveness. Set to -1 to disable jump detection.
 */
@property (nonatomic, strong, nonnull) NSNumber *jumpsAllowed;

/**
 * If the side angle of the face jumps by more than this amount between frames then we have a jump.
 */
@property (nonatomic, strong, nonnull) NSNumber *jumpsMaxXDiff;

/**
 * If the up/down angle of the face jumps by more than this amount between frames then we have a jump.
 */
@property (nonatomic, strong, nonnull) NSNumber *jumpsMaxYDiff;

@end
