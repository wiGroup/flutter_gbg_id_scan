//
//  IDSLivenessDetection.h
//  IDSLiveness
//
//  Created by Edvardas Maslauskas on 23/04/2018.
//  Copyright © 2018 GB Group plc ('GBG'). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "IDSLivenessActionState.h"
#import "IDSLivenessResult.h"
#import "IDSLivenessConfig.h"

/**
 Main interface for liveness detection
 */
@interface IDSLivenessDetection : NSObject

/**
 Block listener, which will be called once liveness result outcome is ready
 */
@property (nonatomic, copy) void (^ _Nonnull onResult)(IDSLivenessResult * _Nonnull result);

/**
 Block listener, which will notify when required liveness action needs to be performed
 */
@property (nonatomic, copy) void (^ _Nonnull onActionChanged)(IDSLivenessActionState * _Nonnull action);

/**
 Block listener, which will pass face images while performing liveness detection.
 It could be used to do the face match in the meantime while doing liveness. The number of calls will depend on the liveness engine configuration
 @see IDSLivenessConfig numberOfFaceImages
 */
@property (nonatomic, copy) void (^ _Nonnull onFaceImage)(UIImage * _Nonnull image);

/**
 Block listener, which will pass required actions for person in order to adjust initial device position to correct the pose
 In order to start performing liveness it's required to hold devide in front of the face +-20 degress from the center
 */
@property (nonatomic, copy) void (^ _Nonnull onPoseAdjustment)(CGPoint pose, BOOL faceSeen, BOOL poseError, NSNumber* _Nonnull poseCounter, NSNumber* _Nonnull poseCounterLimit, NSNumber* _Nonnull actionTested, NSNumber* _Nonnull passed);

/**
 A block listener, which will be notified once liveness is finished loading
 @discussion currently liveness initialisation takes a lot of time, hence we need to init in the
 background in order to not hold the main thread
 */
@property (nonatomic, copy) void (^ _Nonnull onInitialisationDone)(void);

/**
 Main liveness engine constructor

 @param builderBlock a block where you can setup liveness configuration
 @return constructed IDSLivenessDetection object
 */
- (instancetype _Nonnull )initWithConfigBuilder:(void (^_Nonnull)(IDSLivenessConfig *_Nonnull))builderBlock;

/**
 A method, which allows to update liveness engine configuration

 @param config updated configuration
 */
- (void)updateLivenessConfiguration:(IDSLivenessConfig *_Nonnull)config;

/**
 A method, which needs to be called in order to start liveness
 */
- (void)startLivenes;

/**
 A method where frames need to be passed for liveness detection

 @param image that should contain person's face
 */
- (void)performLiveness:(UIImage *_Nonnull)image;

@end
