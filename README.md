# flutter_gbg_id_scan

Flutter plugin implementation if the GBG ID SCAN Native android and ios sdks

## Getting Started

- cd ios/ pod install
- Make Sure your Project has iOS Deployment Target 11. (Runner/Info/iOS Deploymeny Target)
- Update your Project Podfile to '11.0' : platform :ios, '11.0'
- Run project.
If IDES-default-profile.zip not included in your main project, please add

SDK Update:

Android : Update gradle dependencies

IOS:
Open example ios

1. Copy and replace new frameworks in the example project folder
2. Drag new frameworks from the example project to the ios project under Flutter Section, Copy items if needed

 - ReadID_UI.framework
 - ReadID.framework
 - MJCS.framework

    -Profile File should be named IDES-default-profile.zip
    -Check localizable-spot.strings and compare changes with updated sdk Localizable.strings
 3. Download other dependencies : 

 On General Tab Frameworks, Libraries and Embedded , Add Other, select the git repo