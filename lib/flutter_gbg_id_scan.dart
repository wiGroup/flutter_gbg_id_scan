import 'dart:async';

import 'package:flutter/services.dart';

const GbgUserCancelledErrorCode = "499";
const GbgSessionTimeoutErrorCode = "2002";

class GbgConfig {
  final String baseUrl;
  final String username;
  final String password;
  final String journeyId;

  GbgConfig({
    this.baseUrl,
    this.username,
    this.password,
    this.journeyId,
  });
}

class FlutterGbgIdScan {
  final String journeyId;
  final GbgConfig config;

  static const MethodChannel _channel =
      const MethodChannel('com.spot.plugin/gbgidscan');

  FlutterGbgIdScan(this.config, this.journeyId);

  Map<String, String> get credentials => {
        'baseUrl': config.baseUrl,
        'username': config.username,
        'password': config.password,
        'journeyId': journeyId,
      };

  /// return journey ID
  Future<String> get startUserJourney {
    return _channel.invokeMethod('startUserJourney', credentials);
  }
}
