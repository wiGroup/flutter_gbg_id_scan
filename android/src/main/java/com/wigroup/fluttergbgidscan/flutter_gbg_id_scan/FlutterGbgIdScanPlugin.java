package com.wigroup.fluttergbgidscan.flutter_gbg_id_scan;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.idscan.mjcs.MJCS;

import java.io.IOException;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** FlutterGbgIdScanPlugin */
public class FlutterGbgIdScanPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware{
  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    final MethodChannel channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "com.spot.plugin/gbgidscan");
    channel.setMethodCallHandler(new FlutterGbgIdScanPlugin());
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "com.spot.plugin/gbgidscan");
    channel.setMethodCallHandler(new FlutterGbgIdScanPlugin());
    if(FlutterGbgIdScanPlugin.activity == null){
      FlutterGbgIdScanPlugin.activity = registrar.activity();
    }
  }

  private static Activity activity;

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if(call.method.equals("initSdk")){
      try {
        MJCS.init(activity);
      } catch (IOException e) {
        e.printStackTrace();
      }
      result.success("Initialised sdk");
    } else if (call.method.equals("scanDocument")){
      //GBGIDScanModule.scanDocument(activity, call, result);
      result.success("Test response, not implemented");
    }else if (call.method.equals("startUserJourney")){
      try {
        MJCS.init(activity);
        GBGIDScanModule.getInstance().startUserJourney(activity, call, result);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(FlutterPluginBinding binding) {}


  @Override
  public void onAttachedToActivity(ActivityPluginBinding binding) {
    if(FlutterGbgIdScanPlugin.activity == null){
      FlutterGbgIdScanPlugin.activity = binding.getActivity();
    }
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {

  }

  @Override
  public void onReattachedToActivityForConfigChanges(ActivityPluginBinding binding) {}

  @Override
  public void onDetachedFromActivity() {}
}
