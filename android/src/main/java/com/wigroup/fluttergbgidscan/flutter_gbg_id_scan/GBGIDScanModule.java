package com.wigroup.fluttergbgidscan.flutter_gbg_id_scan;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;

import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.wigroup.fluttergbgidscan.flutter_gbg_id_scan.Components.GBGScanner;
import com.wigroup.fluttergbgidscan.flutter_gbg_id_scan.Components.UserJourney;


import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel.Result;

public class GBGIDScanModule {

    private static final int REQUEST_PERMISSIONS = 101;
    private static GBGIDScanModule INSTANCE = null;

    enum Action {
        SCAN_ID,
        USER_JOURNEY
    }

    public static GBGIDScanModule getInstance(){
        if(INSTANCE == null){
            INSTANCE = new GBGIDScanModule();
        }
        return INSTANCE;
    }

    public void scanDocument(Activity activity, MethodCall call, Result result){
        checkPermissions(activity, Action.SCAN_ID, call, result);
    }

    public void startUserJourney(Activity activity, MethodCall call, Result result){
        checkPermissions(activity, Action. USER_JOURNEY, call, result);
    }

    /**
     * Checks for needed permissions for API =< 23.
     */
    @TargetApi(23)
    private void checkPermissions(Activity activity, Action action, MethodCall call, Result result) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            boolean grantedAll = ContextCompat.checkSelfPermission(activity.getApplicationContext(),
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
            if (!grantedAll) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            } else {
                performAction(activity, action, call, result);
            }
        } else {
            performAction(activity, action, call, result);
        }
    }

    private void performAction(Activity activity, Action action, MethodCall call, Result result){
        if(action == Action.SCAN_ID){
            GBGScanner.getInstance(activity,call).startSmartCapture(result);
        }else if(action == Action. USER_JOURNEY){

            UserJourney.Companion.getInstance(activity, call).startCustomerJourney(result);
        }
    }
}
