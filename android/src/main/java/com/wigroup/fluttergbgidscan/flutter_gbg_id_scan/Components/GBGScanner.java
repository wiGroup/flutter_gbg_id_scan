package com.wigroup.fluttergbgidscan.flutter_gbg_id_scan.Components;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.gbgroup.idscan.bento.enterprice.Credentials;
import com.gbgroup.idscan.bento.enterprice.EnterpriseService;
import com.gbgroup.idscan.bento.enterprice.OnDocumentSubmittedListener;
import com.gbgroup.idscan.bento.enterprice.RequestDocumentSend;
import com.gbgroup.idscan.bento.enterprice.ResponseUpload;
import com.idscan.ides.Document;
import com.idscan.mjcs.scanner.DocumentScannerActivity;
import com.idscan.mjcs.scanner.DocumentScannerEventReceiver;
import com.idscan.mjcs.scanner.DocumentScannerService;
import com.idscan.mjcs.scanner.ScannerError;
import com.wigroup.fluttergbgidscan.flutter_gbg_id_scan.R;

import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;



import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel.Result;

public class GBGScanner {

    public static final String PREF_KEY_SCANNER_TYPE = "scanner_type";
    public static final String PREF_KEY_TRIGGER_SHOW = "trigger_show";
    public static final String PREF_KEY_INFO_SHOW = "ui_info";
    public static final String PREF_KEY_SWITCH_SHOW = "ui_switch";
    public static final String PREF_KEY_POPUP_ENABLE = "popup_enable";
    public static final String PREF_KEY_POPUP_DISMISS = "popup_dismiss";
    public static final String PREF_KEY_POPUP_MESSAGE = "popup_message";

    private Bitmap mDocumentImage;
    private static Activity activity;
    private static MethodCall call;
    private static GBGScanner INSTANCE = null;
    private static Result result;

    public GBGScanner(Activity activity, MethodCall call) {
        GBGScanner.activity = activity;
        GBGScanner.call = call;
    }

    public static GBGScanner getInstance(Activity activity, MethodCall call){
        if(INSTANCE == null){
            INSTANCE = new GBGScanner(activity, call);
        }
        return INSTANCE;
    }

    // Used for getting Smart Capture Response.
    private DocumentScannerEventReceiver mEventReceiver = new DocumentScannerEventReceiver() {
        @Override
        public void onProcessingCompleted(@NotNull Context context, Document document) {
            // Background document processing has successfully finished
            mDocumentImage = document.getDocumentImage();
            uploadDocument();
        }

        @Override
        public void onProcessingFailed(@NotNull Context context, ScannerError error) {
            // Background document processing has failed, most possibly camera was failed to open or permissions were not provided.
            Log.d("DOCUMENTSCANNER", "FAILED");
        }

        @Override
        public void onProcessingCancelled(@NotNull Context context,@NotNull  ScannerError scannerError) {
            super.onProcessingCancelled(context, scannerError);
            Log.d("DOCUMENTSCANNER", "CANCELLED");
        }
    };

    // Used for receiving Enterprise service callbacks.
    private OnDocumentSubmittedListener mOnDocumentSubmitListener = new OnDocumentSubmittedListener() {
        @Override
        public void onDocumentUploadedResponse(@Nullable ResponseUpload responseUpload) {
            if (responseUpload != null) {
                //Log.d("DOCUMENTSCANNER RESULT", buildMessageWeb(responseUpload));
                showAlertMessage("Scan Result", buildMessageWeb(responseUpload));
                GBGScanner.result.success(buildMessageWeb(responseUpload));
            } else {
                Log.d("DOCUMENTSCANNER ERR", "Empty response");
            }
        }

        @Override
        public void onError(int code,@NotNull  String message) {
            Log.d("DOCUMENTSCANNER ERR", "Server Error");
        }
    };

    private void showAlertMessage(String title, String message) {
        if (!activity.isDestroyed()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setNeutralButton("ok", null);
            builder.create().show();
        }
    }

    // Showcase of simple upload to server, read JavaDocs and Technical guide for additional settings.
    private void uploadDocument() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        if (mDocumentImage == null) {
            Toast.makeText(activity, "No image", Toast.LENGTH_LONG).show();
            return;
        }
        if (DocumentScannerActivity.SCANNER_TYPE_SELFIE == getScannerType(preferences)) {
            Toast.makeText(activity, "Selfie ", Toast.LENGTH_LONG).show();
            return;
        }

        String baseUrl = Objects.requireNonNull(call.argument("baseUrl")).toString();
        String username = Objects.requireNonNull(call.argument("username")).toString();
        String password = Objects.requireNonNull(call.argument("password")).toString();

        // TODO Credentials, replace credentials whit your own or use Settings screen.
        Credentials credentials = new Credentials(username, password);
        if (!credentials.getUsername().equals("user") && !credentials.getPassword().equals("password")) {
            if (baseUrl != null) {
                Toast.makeText(activity, "Loading ", Toast.LENGTH_LONG).show();
                // Create Request, depending on Action it will differ, this is simple document upload example.
                RequestDocumentSend requestDocumentSend = new RequestDocumentSend()
                        .setCredentials(credentials)
                        .setDocumentImage(mDocumentImage)
                        .setSource(getScannerType(preferences) == DocumentScannerActivity.SCANNER_TYPE_STANDARD ? RequestDocumentSend.Source.ID_SCANNER : RequestDocumentSend.Source.A4_SCANNER);
                new EnterpriseService(activity)
                        .setBaseUrl(baseUrl)
                        .submitDocument(requestDocumentSend, mOnDocumentSubmitListener);
            } else {
                Toast.makeText(activity, "Base URL not set", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(activity, "No credentials or default ones", Toast.LENGTH_LONG).show();
        }
    }

    // Smart capture start, by default all options are OPTIONAL.
    public void startSmartCapture(Result result) {
        GBGScanner.result = result;
        DocumentScannerService.registerEventReceiver(activity, mEventReceiver);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);

        Intent intent = new Intent(activity, DocumentScannerActivity.class);
        // All extra values bellow are optional !!!
        // Default Standard scanner is for Identity Document scanning and alternative ADDRESS_OF_PROOF is for A4.
        //intent.putExtra(DocumentScannerActivity.EXTRA_SCANNER_TYPE, getScannerType(preferences));
        intent.putExtra(DocumentScannerActivity.EXTRA_SCANNER_TYPE, DocumentScannerActivity.SCANNER_TYPE_STANDARD);
        // UI element settings
        intent.putExtra(DocumentScannerActivity.EXTRA_CAMERA_TRIGGER, 12000);
        // If you activate Help button add message for alert dialog to show that text on press.
        intent.putExtra(DocumentScannerActivity.EXTRA_CAMERA_HELP_IS_VISIBLE, preferences.getBoolean(PREF_KEY_INFO_SHOW, false));
        intent.putExtra(DocumentScannerActivity.EXTRA_CAMERA_SWITCH_IS_VISIBLE, preferences.getBoolean(PREF_KEY_SWITCH_SHOW, false));

        // Dialog
        intent.putExtra(DocumentScannerActivity.EXTRA_ALERT_DIALOG, preferences.getBoolean(PREF_KEY_POPUP_ENABLE, DocumentScannerActivity.ALERT_DIALOG_HIDDEN));
        intent.putExtra(DocumentScannerActivity.EXTRA_ALERT_DIALOG_SHOW_TIME, Integer.parseInt(preferences.getString(PREF_KEY_POPUP_DISMISS, "10000")));
        intent.putExtra(DocumentScannerActivity.EXTRA_ALERT_DIALOG_MESSAGE, preferences.getString(PREF_KEY_POPUP_MESSAGE, activity.getString(R.string.action_required_scanning_message)));

        activity.startActivity(intent);
    }

    // Small example of results from the WebService.
    public String buildMessageWeb(ResponseUpload result) {
        // All possible keys are in IEOS Api Appendix reference guide.
        String firstNameKey = "ExtractedFields.FirstName";
        String lastNameKey = "ExtractedFields.LastName";
        String documentTypeKey = "DocumentType";
        StringBuilder builder = new StringBuilder();
        builder.append("Scan Reference").append(result.getReferenceNumber()).append('\n');
        Map<String, String> entryData = result.getEntryData();
        if (entryData != null && !entryData.isEmpty()) {
            if (entryData.get(firstNameKey) != null) {
                builder.append("First Name").append(
                        entryData.get(firstNameKey)).append('\n');
            }
            if (entryData.get(lastNameKey) != null) {
                builder.append("Last Name").append(
                        entryData.get(lastNameKey)).append('\n');
            }
            if (entryData.get(documentTypeKey) != null) {
                builder.append("Document Type").append(entryData.get(documentTypeKey)).append('\n');
            }
        }
        return builder.toString();
    }

    // Small example of results from the WebService.
    public String buildMessageAutoCapture(Document document) {
        return "Document Name" + " " + document.getName() + '\n' +
                "Is recognised" + " " + document.isRecognised() + '\n';
    }

    private int getScannerType(SharedPreferences preferences){
        return Integer.parseInt(preferences.getString(PREF_KEY_SCANNER_TYPE, String.valueOf(DocumentScannerActivity.SCANNER_TYPE_STANDARD)));
    }
    //EndRegion Scan Document
}
