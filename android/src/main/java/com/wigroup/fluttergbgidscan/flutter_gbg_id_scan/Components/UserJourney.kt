package com.wigroup.fluttergbgidscan.flutter_gbg_id_scan.Components

import android.app.Activity
import android.content.Context
import android.text.TextUtils
import android.widget.Toast
import com.gbgroup.idscan.bento.enterprice.Credentials
import com.gbgroup.idscan.bento.enterprice.response.ResponseJourney
import com.gbgroup.idscan.bento.enterprice.response.data.ResponseVersionInfo
import com.idscan.mjcs.customerJourney.CustomerJourneyActivity
import com.idscan.mjcs.customerJourney.CustomerJourneyConfig
import com.idscan.mjcs.customerJourney.CustomerJourneyError
import com.idscan.mjcs.customerJourney.CustomerJourneyEventReceiver
import com.idscan.mjcs.customerJourney.CustomerJourneyService.registerEventReceiver
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import java.util.*

class UserJourney(activity: Activity?, call: MethodCall?) {
    private var result: MethodChannel.Result? = null
    private var resultSubmitted = false //Adding this as callback from CustomerJourneyEventReceiver can come twice. Maybe a bug?

    fun getInstance(activity: Activity?, call: MethodCall?): UserJourney? {
        if (INSTANCE == null) {
            INSTANCE = UserJourney(activity, call)
        }
        return INSTANCE
    }
    fun startCustomerJourney(result: MethodChannel.Result?) {
        INSTANCE!!.result = result
        resultSubmitted = false
        val baseUrl = Objects.requireNonNull(call!!.argument<Any>("baseUrl")).toString()
        val username = Objects.requireNonNull(call!!.argument<Any>("username")).toString()
        val password = Objects.requireNonNull(call!!.argument<Any>("password")).toString()
        val journeyId = Objects.requireNonNull(call!!.argument<Any>("journeyId")).toString()

        // Register event receiver.
        registerEventReceiver(activity!!, mCustomerJourneyEventReceiver)
        if (TextUtils.isEmpty(baseUrl) || TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            Toast.makeText(activity, "Base URL, username and password required", Toast.LENGTH_LONG).show()
            return
        }

        val config = CustomerJourneyConfig.Builder(baseUrl,
                Credentials(username, password)).setCertificates(emptyList())
                .setJourneyDefinitionId(journeyId)
                .build()

        CustomerJourneyActivity.startActivity(activity!!, config)

    }

    // Response event receiver.
    private val mCustomerJourneyEventReceiver: CustomerJourneyEventReceiver = object : CustomerJourneyEventReceiver() {
        override fun onServerInfo(context: Context, responseVersionInfo: ResponseVersionInfo) {}
        override fun onJourneyCompleted(context: Context, responseJourney: ResponseJourney) {
            if (!resultSubmitted) {
                resultSubmitted = true
                result!!.success(responseJourney.journeyId)
            }
        }

        // This is called when we deem that we could not restore our state, this can indicate camera error, webservice...
        override fun onJourneyFailed(context: Context, errorCode: CustomerJourneyError) {
            if (!resultSubmitted) {
                resultSubmitted = true
                result!!.error(ERROR_JOURNEY_FAILED, ERROR_JOURNEY_FAILED, parseCustomerJourneyError(errorCode))
            }
        }

        // This is called when user cancels journey by onBack() event or uses cancel.
        override fun onJourneyCanceled(context: Context, error: CustomerJourneyError) {
            if (!resultSubmitted) {
                resultSubmitted = true
                result!!.error(USER_CANCELLED_CODE, ERROR_JOURNEY_CANCELLED, parseCustomerJourneyError(error))
            }
        }
    }

    private fun parseCustomerJourneyError(error: CustomerJourneyError): List<Map<String, String>> {
        val data: MutableList<Map<String, String>> = ArrayList()
        // Error Message
        var item: MutableMap<String, String> = HashMap()
        item[MAP_KEY] = "Error Message"
        item[MAP_VALUE] = error.message
        data.add(item)

        // Error Code look up in CustomerJourneyError object all possible codes supported + HTTPS code and 9999 for generic webservice failing check messages for that.
        item = HashMap()
        item[MAP_KEY] = "Error Code"
        item[MAP_VALUE] = error.code.toString()
        data.add(item)

        // Will return journeyID if journey was started if not the object will be null.
        item = HashMap()
        item[MAP_KEY] = "Journey ID"
        item[MAP_VALUE] = error.journeyId
        data.add(item)
        return data
    }

    companion object {
        private const val MAP_KEY = "key"
        private const val MAP_VALUE = "value"
        private var activity: Activity? = null
        private var call: MethodCall? = null
        private var INSTANCE: UserJourney? = null

        //Error handling messages
        private const val ERROR_JOURNEY_FAILED = "User Journey Failed."
        private const val ERROR_JOURNEY_CANCELLED = "User Journey Cancelled."
        private const val USER_CANCELLED_CODE = "499"
        private const val SESSION_TIMEOUT_CODE = "2002"
        fun getInstance(activity: Activity?, call: MethodCall?): UserJourney? {
            if (INSTANCE == null) {
                INSTANCE = UserJourney(activity, call)
            }
            return INSTANCE
        }
    }

    init {
        Companion.activity = activity
        Companion.call = call
    }
}